<?php
    /**
    * Page Content Template
    *
    * This template is the default page content template. It is used to display the content of the
    * `page.php` template file, contextually, as well as in archive lists or search results.
    *
    * @package WooFramework
    * @subpackage Template
    */

    /**
    * Settings for this template file.
    *
    * This is where the specify the HTML tags for the title.
    * These options can be filtered via a child theme.
    *
    * @link http://codex.wordpress.org/Plugin_API#Filters
    */
    global $woo_options;

    $heading_tag = 'h1';
    if ( is_front_page() ) { $heading_tag = 'h2'; }
    $title_before = '<' . $heading_tag . ' class="title entry-title">';
    $title_after = '</' . $heading_tag . '>';

    $page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

    woo_post_before();
?>
<article <?php post_class(); ?>>
    <?php
        woo_post_inside_before();
    ?>
    <?php 

        //this var created by our FRN plugin. It's used to trigger mobile features.
        global $frn_mobile;

    if (has_post_thumbnail( $post->ID ) ){ ?>
        <?php 
            
            $image_id = get_post_thumbnail_id( $post->ID );
            
            //This first checks to see if the var is defined. It helps if we ever move off or own mobile detection.
            if(isset($frn_mobile)) {
                if($frn_mobile=="Tablet") {
                    $image = wp_get_attachment_image_src( $image_id, 'large' ); 
                    //$img_srcset = wp_get_attachment_image_srcset( $image_id, 'large' );
                }
                elseif($frn_mobile=="Smartphone") {
                    $image = wp_get_attachment_image_src( $image_id, 'medium' ); 
                    //$img_srcset = wp_get_attachment_image_srcset( $image_id, 'medium' );
                }
                else {
                    $image = wp_get_attachment_image_src( $image_id, 'full' ); 
                    //$img_srcset = wp_get_attachment_image_srcset( $image_id, 'full' );
                }
            } 
            else {
                $image = wp_get_attachment_image_src( $image_id, 'full' ); 
                //$img_srcset = wp_get_attachment_image_srcset( $image_id, 'full' );
            }

            $imgback = $image[0];

        }else{

            //Default: "http://thecanyonmalibu.com/wp-content/uploads/malibu-california-desert-flower-plant.jpg";
            //OLD: trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/article-back.jpg";

            //This first checks to see if the var is defined. It helps if we ever move off or own mobile detection.
            if(isset($frn_mobile)) {
                //echo "<h2>FRN_Mobile: ".$frn_mobile."</h2>";
                if($frn_mobile=="Tablet") {
                    $image = wp_get_attachment_image_src( '7906', 'large' ); 
                    //$img_srcset = wp_get_attachment_image_srcset( '7906', 'large' );
                }
                elseif($frn_mobile=="Smartphone") {
                    $image = wp_get_attachment_image_src( '7906', 'medium' ); 
                    //$img_srcset = wp_get_attachment_image_srcset( '7906', 'medium' );
                }
                else {
                    $image = wp_get_attachment_image_src( '7906', 'full' ); 
                    //$img_srcset = wp_get_attachment_image_srcset( '7906', 'full' );
                }
            } 
            else {
                $image = wp_get_attachment_image_src( '7906', 'full' ); 
                //$img_srcset = wp_get_attachment_image_srcset( '7906', 'full' );
            }

            $imgback = $image[0];
            //$img_srcset = "";

    } ?>
    <ul class="breadcrumb">
    <?php bcn_display_list(); ?>
    </ul>
    <header class="article-back" style="background: url('<?php echo $imgback; ?>') no-repeat center center / cover">
    <div class="row">
    <div class="large-12 columns">
        <?php the_title( $title_before, $title_after ); ?>
    </div>
    </div>
    </header>

    <?php if(is_page('welcome')){ ?>
    <section class="entry aboutauthore_welcome">
    <?php } else {  ?>
    <section class="entry">
    <!--<div class=""></div>  -->
    <?php } ?>
        <?php
            if ( ! is_singular() ) {
                the_excerpt();
            } else {
                the_content( __( 'Continue Reading &rarr;', 'woothemes' ) );
            }
            wp_link_pages( $page_link_args );
        ?>
    <?php if(is_page('testimonials')){ ?>
    <?php $argstestimonials = array('post_type' => 'testimonial','posts_per_page' =>'-1');
          $looptestimonials = new WP_Query( $argstestimonials );    
          $j= 1;
          while ( $looptestimonials->have_posts() ) : $looptestimonials->the_post(); 
          if($j%2){ ?>
    <div class="box lblue">
    <?php } else { ?>
    <div class="box gray">
    <?php } ?>
    <em>" <?php echo get_the_content(); ?>"</em>
    <p>-<?php echo get_the_title(); ?></p>
    </div>
    <?php $j++; endwhile; } ?>  
    </section><!-- /.entry -->
    <?php if(is_page('welcome')){ ?>
    <div class="fix"></div>
    <section class="entry  large-12 aboutauthor_section">    
    <div class="aboutkath">
    <?php echo get_field('about_author_content_section'); ?>
    </div> 
    </section>    
    <?php } ?>
    <div class="fix"></div>
    <?php
        woo_post_inside_after();
    ?>
</article><!-- /.post -->
<?php
    woo_post_after();
    $comm = get_option( 'woo_comments' );
    if ( ( $comm == 'page' || $comm == 'both' ) && is_page() ) { comments_template(); }
?>