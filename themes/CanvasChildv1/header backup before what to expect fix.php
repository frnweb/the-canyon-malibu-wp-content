<?php
    /**
    * Header Template
    *
    * Here we setup all logic and XHTML that is required for the header section of all screens.
    *
    * @package WooFramework
    * @subpackage Template
    */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head> 
    <script src="https://use.typekit.net/ste2ila.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
    <title><?php woo_title(); ?></title>
    <?php woo_meta(); ?>
    <?php //echo '   <link rel="pingback" href="'.esc_url( get_bloginfo( 'pingback_url' ) ).'" />'; ?>
        
    <link href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/css/customcp.css" rel="stylesheet">
    <?php wp_head(); ?>
    <?php woo_head(); ?>

    <meta property="og:image" content="/wp-content/uploads/the-canyon-social-sharing-default-banner.jpg" />
    <script src="https://use.typekit.net/zng4vsp.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <style type="text/css">
        .wf-loading {
            font-family: "open-sans";
            visibility: hidden;
        }
        .wf-active {
            visibility: visible;
        }
        #html5-watermark {
            display: none !important;
        }
    </style>
    <script type="text/javascript">
        /*var html5lightbox_options = {
            watermark: "",
            watermarklink: ""
        }; */
    </script>
    <!--[if IE]>
    <style>
    #header .mega-menu-sec ul.mega-menu li.mega-right-menu {   {
    margin-top: -75px !important;
    }
    </style>
    <![endif]-->
</head>
<body <?php body_class(); ?>>
<?php woo_top(); ?>
<div id="wrapper">

<div id="inner-wrapper">

    <?php woo_header_before();
        $settings = woo_get_dynamic_values( array( 'logo' => '' ) );
        $site_title = get_bloginfo( 'name' );
        $site_url = home_url( '/' );
        $site_description = get_bloginfo( 'description' );
    ?>	
    <div id="mobile-header" class="mobile-header col-full" style="display: none;">  
        <div class="mega-menu-sec"><?php echo do_shortcode('[maxmegamenu location=max_mega_menu_1]'); ?></div>
	<div class="mobile-header-icon"><a title="<?php echo esc_attr( $site_title ); ?>" href="<?php echo esc_url( $site_url ); ?>"><img src="http://thecanyonmalibu.com/wp-content/uploads/Canyon-Mobile-Header-Icon.png" alt="The Canyon Malibu"></div></a>   
        <div class = "frnphonenumber"><div><h2><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header"]'); ?></h2></div> </div> 
        <?php
            if ( ( '' != $settings['logo'] ) ) {
                $logo_url = $settings['logo'];
                if ( is_ssl() ) $logo_url = str_replace( 'http://', 'https://', $logo_url );
            ?>
            <div id="logo">
                <a title="<?php echo esc_attr( $site_title ); ?>" href="<?php echo esc_url( $site_url ); ?>"><img alt="<?php echo esc_attr( $site_title ); ?>" src="<?php echo esc_url( $logo_url ); ?>"></a>
            </div>
            <?php
            }
        ?>
    </div>
    <header id="header" class="col-full">
        <div class="columns large-2 logo_sec small-2">
            <?php woo_header_inside(); ?>
        </div>
        <div class="columns large-6 after-header"><?php woo_header_after(); ?>  </div>
        <div class="columns large-4 contact-info small-8">
            <div class = "frnphonenumber small-6 large-4"><div><h2><i class="fa fa-phone"></i><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header"]'); ?></h2></div> </div>	
            <div class="single-menu small-6 large-6"><a href="http://thecanyonmalibu.com/treatment-admissions/">What to expect</a></div>
            <!--<div class="mega-menu"><?php //echo do_shortcode('[maxmegamenu location=max_mega_menu_1]'); ?></div>  -->
        </div>
        <div class="mega-menu-sec"><?php echo do_shortcode('[maxmegamenu location=max_mega_menu_1]'); ?></div>      
    </header>
	</div>