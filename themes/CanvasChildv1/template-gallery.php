<?php
    /**
    * Template Name: Gallery Page
    *
    * @package WooFramework
    * @subpackage Template
    */
    get_header(); 
?>
<ul class="breadcrumb">
                <?php bcn_display_list(); ?>
</ul>
<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full"> 
<h2 class="gallery-title">Photo Gallery</h2>
<?php
    the_content();
?> 
</div><!-- /#content -->  
<?php woo_content_after(); ?>

<?php get_footer(); ?>