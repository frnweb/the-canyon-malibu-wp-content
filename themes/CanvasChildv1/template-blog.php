<?php
    /**
    * Template Name: Blog
    *
    * The blog page template displays the "blog-style" template on a sub-page.
    *
    * @package WooFramework
    * @subpackage Template
    */

    get_header();
    global $woo_options;   
?>
<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full">

    <div id="main-sidebar-container">
        <?php
            if (has_post_thumbnail( get_option( 'page_for_posts' )) ){ 
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_option( 'page_for_posts' ) ), 'full' ); 
                $imgback = $image[0];
            }else{

                $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/blog-back.jpg";   
        } ?>
        <ul class="breadcrumb">
            <?php bcn_display_list(); ?>
        </ul>
        <header class="article-back" style="background: url('<?php echo $imgback;  ?>') no-repeat center center / cover ">
            <div class="row">
                <div class="large-12 columns">
                    <h1 class="title entry-title"><?php echo get_the_title(get_option( 'page_for_posts' ));  ?></h1>
                </div>
            </div>
        </header>
        <div class="nav-blog-menu">
            <label for="show-menu" class="show-menu">Show Menu</label>
            <input type="checkbox" id="show-menu" role="button">
            <?php wp_nav_menu ('blog-menu'); ?>
        </div>
        <!-- #main Starts -->
        <?php woo_main_before(); ?>

        <section id="main" class="col-left">

            <?php get_template_part( 'loop', 'blog' ); ?>

        </section><!-- /#main -->
        <?php woo_main_after(); ?>

        <?php get_sidebar(); ?>

    </div><!-- /#main-sidebar-container -->

    <?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>

<?php get_footer(); ?>