<?php
/*
Template Name: FRN Formatting
*/
    get_header(); 
?>
<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full"> 
    <?php
        if (has_post_thumbnail($post->ID) ){ 
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
            $imgback = $image[0];
        }else{

            $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/blog-back.jpg";   
        }
    ?>
    <ul class="breadcrumb">
    <?php bcn_display_list(); ?>
    </ul>
    <header class="article-back" style="background: url('<?php echo $imgback;  ?>') no-repeat center center / cover ">
        <div class="row">
            <div class="large-12 columns">
                <h1 class="title entry-title"><?php echo get_the_title($post->ID );  ?></h1>
            </div>
        </div>
    </header>
<div class="row">
<div class="large-12 columns">
<?php 
// This is where all of the sample formatting stuff goes
// Please note the code in the <textarea> requires no formatting/spacing tabs.
?>

<style>code{background: none; border: none;}textarea{overflow-y: scroll; width: 100%;}</style>
<br>

<div class="row">
	<div class="large-6 columns">
		<h1>h1. This is a very large header.</h1>
		<h2>h2. This is a large header.</h2>
		<h3>h3. This is a medium header.</h3>
		<h4>h4. This is a moderate header.</h4>
		<h5>h5. This is a small header.</h5>
		<h6>h6. This is a tiny header.</h6>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 200px;">
<h1>h1. This is a very large header.</h1>
<h2>h2. This is a large header.</h2>
<h3>h3. This is a medium header.</h3>
<h4>h4. This is a moderate header.</h4>
<h5>h5. This is a small header.</h5>
<h6>h6. This is a tiny header.</h6>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">

<div class="divider"></div>

	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 30px;">
[divider]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->
</div><!-- /.row -->


<div class="row">
	<div class="small-12 columns">
			<code class="clearfix">
			<textarea style="height: 500px;">
[row]
[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]
[/row]
		</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
		<blockquote class="secondary">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 125px;">
<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.”
<cite>Noah benShea</cite></blockquote>
<blockquote class="secondary">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Un-ordered Lists</h4>
		<ul>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="secondary">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="disc">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Un-ordered Lists</h4>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="secondary">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="disc">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Ordered Lists</h4>
		<ol>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ol>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Ordered Lists</h4>
<ol>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ol>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="callout no-box">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="callout no-box-large">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 700px;">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box-large"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<div class="callout">
			<h4>H4. This is the Default Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="callout secondary">
			<h4>H4. This is the Secondary Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="callout border">
			<h4>H4. This is a Border Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="callout-card">
			<div class="card-media" style="background-image: url(/wp-content/themes/CanvasChildv1/assets/images/thecanyon-facility.jpg )"></div>
			<div class="card-content">
				<h4>This is the Default Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="callout-card secondary">
			<div class="card-media" style="background-image: url(/wp-content/themes/CanvasChildv1/assets/images/thecanyon-facility.jpg )"></div>
			<div class="card-content">
				<h4>This is a Secondary Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="callout-card border">
			<div class="card-media" style="background-image: url(/wp-content/themes/CanvasChildv1/assets/images/thecanyon-facility.jpg )"></div>
			<div class="card-content">
				<h4>This is a Border Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 1000px;">
[callout]
<h4>H4. This is the Default Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="secondary"]
<h4>H4. This is the Secondary Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="border"]
<h4>H4. This is a Border Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout-card img="/wp-content/themes/CanvasChildv1/assets/images/thecanyon-facility.jpg"]
<h4>This is the Default Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/themes/CanvasChildv1/assets/images/thecanyon-facility.jpg" style="secondary"]
<h4>This is a Secondary Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/themes/CanvasChildv1/assets/images/thecanyon-facility.jpg" style="border"]
<h4>This is a Border Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-5 columns">
		<p><a href="#" class="buttons " target="_self">Button</a> <a href="#" class="buttons secondary" target="_self">Button</a><a href="#" class="buttons border" target="_self">Button</a></p>
		<div class="read-next">
			<h4>Read This Next:</h4>
			<p><a href="#" class="buttons read-next" target="_self">Long Article Title</a></p>
		</div>
	</div><!-- /.col columns -->
	<div class="large-7 columns">
			<textarea style="height: 200px;">
[button url="#"]Button[/button]
[button url="#" style="secondary"]Button[/button]
[button url="#" style="border"]Button[/button]
[button url="#" style="read-next"]Long Article Title[/button]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

</div><!-- content -->

<?php get_footer(); ?>