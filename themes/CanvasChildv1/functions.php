<?php
    //
    // Recommended way to include parent theme styles.
    //  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
    //  
    add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
    function theme_enqueue_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', '', 1.04);
    }
    //
    // Your code goes below
    //

    // Begin Removing RSS Feeds for comments while keeping the main one available
    // First, we remove all the RSS feed links from wp_head using remove_action
    remove_action( 'wp_head','feed_links', 2 );
    remove_action( 'wp_head','feed_links_extra', 3 );

    // We then need to reinsert the main RSS feed by using add_action to call our function
    add_action( 'wp_head', 'reinsert_rss_feed', 1 );

    // This function will reinsert the main RSS feed *after* the others have been removed
    function reinsert_rss_feed() {
        echo '<link rel="alternate" type="application/rss+xml" title="' . get_bloginfo('sitename') . ' &raquo; RSS Feed" href="' . get_bloginfo('rss2_url') . '" />';
    }
    //

    //
    // Changes Excerpt Length
    //
    add_filter( 'excerpt_length', 'woo_custom_excerpt_length', 200 );
    function woo_custom_excerpt_length ( $length ) {
        if ( is_front_page() ) { $length = 80; }
        return $length;
    } // End woo_custom_excerpt_length()

    //
    // Allows pinch & zoom on iphones/ipad
    //
    function woo_load_responsive_meta_tags () {
        $html = '';

        $html .= "\n" . '<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->' . "\n";
        $html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />' . "\n";

        /* Remove this if not responsive design */
        $html .= "\n" . '<!--  Mobile viewport scale | Disable user zooming as the layout is optimised -->' . "\n";
        $html .= '<meta content="initial-scale=1.0; maximum-scale=4.0; user-scalable=yes" name="viewport"/>' . "\n";

        echo $html;
    } // End woo_load_responsive_meta_tags()




    //////
    // AUTHOR/USER CUSTOMIZATIONS
    //////

    //Add more contact options to users
    function modify_contact_methods($profile_fields) {
        // Add new contact fields
        $profile_fields['linkedin'] = 'LinkedIn URL';
        $profile_fields['slideshare'] = 'SlideShare Profile';
        // Remove old contact fields
        unset($profile_fields['aim']);
        unset($profile_fields['yim']);
        unset($profile_fields['jabber']);

        return $profile_fields;
    }
    add_filter('user_contactmethods', 'modify_contact_methods');

    remove_filter('pre_user_description', 'wp_filter_kses'); //removes HTML filter from author bios
    add_filter( 'pre_user_description', 'wp_filter_post_kses' );  //adds traditional filters to bios like page content has


    // BEGIN: Adds a FRN author settings section at the bottom of user profile settings
    if ( !function_exists('add_frn_author_fields')) {
    function add_frn_author_fields( $user ) {
        $position = esc_attr( get_the_author_meta( 'position', $user->ID ) );
        $company = esc_attr( get_the_author_meta( 'company', $user->ID ) );
        $author_public = get_the_author_meta( 'author_public', $user->ID);
		if(!isset($author_public)) $author_public="";
    ?>
    <h3><?php _e('FRN Author Information'); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="Position Title"><?php _e('Position Title'); ?></label></th>
            <td>
                <input type="text" name="position" id="position" value="<?php echo $position; ?>" class="regular-text" /><span class="description"><?php _e(''); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="Company"><?php _e('Facility/Company'); ?></label></th>
            <td>
                <input type="text" name="company" id="company" value="<?php echo $company; ?>" class="regular-text" /><span class="description"><?php _e(''); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="Make Author Public"><?php _e('Make Author Public'); ?></label></th>
            <td>
                <label><input type="checkbox" name="author_public" <?php if ($author_public == 'Y' ) { ?>checked="checked"<?php }?> value="Y">Yes &nbsp;</label><small class="description"><?php _e('(Activates authorship in posts & pages)'); ?></small>
            </td>
        </tr>
    </table>
    <?php 
	}
    }
    if ( !function_exists('frn_custom_user_profile_fields')) {
	function frn_custom_user_profile_fields( $user_id ) {
		if ( !current_user_can( 'edit_user', $user_id ) )
			return FALSE;
		if(!isset($_POST['author_public'])) $_POST['author_public']="";
		update_user_meta( $user_id, 'position', $_POST['position'] );
		update_user_meta( $user_id, 'company', $_POST['company'] );
		update_user_meta( $user_id, 'author_public', $_POST['author_public'] );
	}
    }

	add_action( 'show_user_profile', 'add_frn_author_fields' );
	add_action( 'edit_user_profile', 'add_frn_author_fields' );
	add_action( 'personal_options_update', 'frn_custom_user_profile_fields' );
	add_action( 'edit_user_profile_update', 'frn_custom_user_profile_fields' );

//END: FRN Author fields


/*
function change_author_permalinks() {
global $wp_rewrite;
$wp_rewrite->author_base = 'author/';
$wp_rewrite->author_structure = '/' . $wp_rewrite->author_base. '%author%';
}
add_action('init','change_author_permalinks');
*/

//
//Allow SVGS to upload through Wordpress Media Uploader
//
function cc_mime_types($mimes) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


////////
// END AUTHOR/USER CUSTOMIZATIONS
////////
function thecanyon_enqueue_style() {
    global $post;
    if ( is_child_theme() ) {
        // load parent stylesheet first if this is a child theme
        wp_enqueue_style( 'custom-stylesheet', trailingslashit( get_stylesheet_directory_uri() ) . 'assets/css/custom.css', true, 1.7 );
        wp_enqueue_style( 'accordation-stylesheet', trailingslashit( get_stylesheet_directory_uri() ) . 'assets/css/accordation.css', true, 1.1 ); 
        wp_enqueue_style( 'scrollpane-stylesheet', trailingslashit( get_stylesheet_directory_uri() ) . 'assets/css/jquery.jscrollpane.css', true, 0.1 ); 
    }
    $post_type = get_post_type();
    if((!is_search()) && (!is_archive()) && (!is_404())){
    
        $post_id = wp_get_post_parent_id( $post->ID );
        $parent_temp = get_post_meta( $post_id , '_wp_page_template', true );
        $currentTemplate = get_post_meta( $post->ID, '_wp_page_template', true );
        if(($currentTemplate == "template-about.php") || ($parent_temp == "template-about.php")){
            $bcrumb = "About";
        }
        elseif(($currentTemplate == "template-contact.php") || ($parent_temp == "template-contact.php")){
            $bcrumb = "Contact";
        }
        else{
            $bcrumb = "";
        }
    }else{
        $bcrumb = "";
    }
    if((is_single()) && ($post_type == "staff")){
       $bcrumb = "About"; 
    }
    $translation_js = array(
    'templateNm' => $bcrumb
    );
    wp_register_script( 'thecanyon_custom_js', trailingslashit( get_stylesheet_directory_uri() )  . 'assets/js/custom.js' );
    wp_localize_script( 'thecanyon_custom_js', 'obj_name', $translation_js );
    wp_enqueue_script( 'thecanyon_custom_js', array(), '', true );
    wp_enqueue_script( 'accordation_js', trailingslashit( get_stylesheet_directory_uri() )  . 'assets/js/accordation.js', array( 'jquery' ), '1.0', true ); 
    
    wp_enqueue_script( 'thecanyon_sticky_js' );
    wp_enqueue_script( 'thecanyon_sticky_js', trailingslashit( get_stylesheet_directory_uri() )  . 'assets/js/jquery.jsticky.min.js', array( 'jquery' ), '1.0', true ); 
    
    wp_enqueue_script( 'mousewheel_js', trailingslashit( get_stylesheet_directory_uri() )  . 'assets/js/jquery.mousewheel.js', array( 'jquery' ), '1.0', true ); 
    wp_enqueue_script( 'scrollpane_js', trailingslashit( get_stylesheet_directory_uri() )  . 'assets/js/jquery.jscrollpane.min.js', array( 'jquery' ), '1.0', true ); 
    wp_enqueue_script( 'videolightbox_js', trailingslashit( get_stylesheet_directory_uri() )  . 'assets/js/html5lightbox.js', array( 'jquery' ), '1.0', true ); 
        wp_enqueue_script( 'thecanyon_custom_js', trailingslashit( get_stylesheet_directory_uri() )  . 'assets/js/custom.js', array( 'jquery' ), '1.0', true );
}

add_action( 'wp_enqueue_scripts', 'thecanyon_enqueue_style' );
function updateNumbers() {
    /* numbering the published posts, starting with 1 for oldest;
    / creates and updates custom field 'incr_number';
    / to show in post (within the loop) use <?php echo get_post_meta($post->ID,'incr_number',true); ?>
    / alchymyth 2010 */
    global $wpdb;
    $querystr = "SELECT $wpdb->posts.* FROM $wpdb->posts 
    WHERE $wpdb->posts.post_status = 'publish' 
    AND $wpdb->posts.post_type = 'post' 
    ORDER BY $wpdb->posts.post_date DESC";
    $pageposts = $wpdb->get_results($querystr, OBJECT);
    $counts = 0 ;
    if ($pageposts):
        foreach ($pageposts as $post):
            $counts++;
            add_post_meta($post->ID, 'incr_number', $counts, true);
            update_post_meta($post->ID, 'incr_number', $counts);
            endforeach;
        endif;
}

add_action ( 'publish_post', 'updateNumbers', 11 );
add_action ( 'deleted_post', 'updateNumbers' );
add_action ( 'edit_post', 'updateNumbers' );

//* Customize [...] in WordPress excerpts
function sp_read_more_custom_excerpt( $text ) {
    if ( strpos( $text, '[&hellip;]') ) {
        $excerpt = str_replace( '[&hellip;]', '...', $text );
    } else {
        $excerpt = $text;
//        $excerpt = $text .'...';
    }
    return $excerpt;
}

function custom_excerpt_length( $length ) {
    $post_type = get_post_type();
    if($post_type == "location"){
        return 20;
    }else{
        return 100;
    }
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function contact_info(){
    $admin_email = get_option( 'admin_email' );    
    $info ="
    <div class='contact-options columns large-12'>
        <div class='columns large-4'>
            <div class='phn-number contact-info'>".do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Page (Contact Us)" image_url="/wp-content/uploads/the-canyon-chat-call-button.gif" alt="Free Assessment / Confidential Call: %%frn_phone%%"]')."</div>
        </div>
        <div class='columns large-4'>
            <div class='phn-number contact-info'>".do_shortcode('[lhn_inpage button="email" url="/wp-content/uploads/the-canyon-chat-email-button.gif"]')."</div>
        </div> 
        <div class='columns large-4'>".do_shortcode('[lhn_inpage button="chat" id="8042"]')."</div> 
    </div>";
    echo $info; 
    //"<i class='fa fa-phone'></i><span>FREE Assessment / Confidential Call</span>""
    //"<i class='mail-icon'></i><span>Send Us A Message</span><a href=mailto:".$admin_email." target='_top'>Email Us</a>"
    //"<i class='chat-icon'></i><span>Talk With Us Online</span><div class='chat contact-info'>Chat Online</div>"
}



//add_theme_support( 'post-thumbnails' );
add_action('init', 'staff_custom_init');
function staff_custom_init() {
    // 'portfolio' is my post type, you replace it with yours
    add_post_type_support( 'staff', 'thumbnail' ); 
}



function catch_that_image() {
    global $post, $posts;
    //echo "<h2>ID : ".$post->ID."</h2>";
    //$images = get_attached_media( 'image' , $post->ID);
    //print_r($images);
    //echo "<br>catching image";
    //Find the first image
    $new_img_tag = ""; $img_srcset=""; $first_img = ''; $image_alt="";
    $output = preg_match_all('/< *img[^>]*src *= *["\']?([^"\']*)/i', $post->post_content, $matches);
    if($output!="0") $first_img = $matches [1][0];

    if(!empty($first_img)){ 
        //echo "<br>first image url found";

        //Get attachment ID for image to pull additional details and make it mobile speed friendly
        //The following code originated: http://wpscholar.com/blog/get-attachment-id-from-wp-image-url/
        $attachment_id = "";
        $dir = wp_upload_dir();
        if ( false !== strpos( $first_img, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
            $file = basename( $first_img );

            //source: https://philipnewcomer.net/2012/11/get-the-attachment-id-from-an-image-url-in-wordpress/
            // If this is the URL of an auto-generated thumbnail, get the URL of the original image
            $file = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $file );
            // Remove the upload path base directory from the attachment URL
            $upload_dir_paths = wp_upload_dir();
            $file = str_replace( $upload_dir_paths['baseurl'] . '/', '', $file );
     
            //echo "<br>File : ".$file;
            if($file=="Wendy.jpg") $hide_image=true;
        
            if(!isset($hide_image)) {
            $query_args = array(
                'post_type'   => 'attachment',
                'post_status' => 'any',
                'fields'      => 'ids',
                'meta_query'  => array(
                    array(
                        'value'   => $file,
                        'compare' => 'LIKE',
                        'key'     => '_wp_attachment_metadata',
                    ),
                )
            );
            $media_query = new WP_Query( $query_args );
            //print_r($media_query);
            if ( $media_query->have_posts() ) {
                $count=0;
                //echo "<br>Found : ".$media_query->post_count;
                while($attachment_id=="" && $count<=$media_query->post_count) {
                //foreach ( $media_query->posts as $post_id ) {
                    //echo "<h2>Set : ".$media_query->posts[0]."</h2>";
                    $meta = wp_get_attachment_metadata( $media_query->posts[0] );
                    $original_file = basename( $meta['file'] );
                    $cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
                    if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
                        $attachment_id = $media_query->posts[0];
                        break;
                    }
                //}
                $count++;
                }
            }
            }
            //else echo "<br>Search for file unsuccessful (<a href='http://thecanyonmalibu.com/wp-admin/upload.php'>try yourself</a>)";

        }
        if(!isset($hide_image)) $hide_image = false;
        if(!empty($attachment_id) && !$hide_image) {
            //echo "<br>attachment ID found: ".$attachment_id;
            $image_id = $attachment_id;
            $image_meta = wp_get_attachment_metadata( $attachment_id );
            //echo "<br>width: ".$image_meta['width'];
            if(isset($image_meta['width'])) {
                if($image_meta['width']<220) $hide_image = true;
            }
            if(!$hide_image) {
                $image = wp_get_attachment_image_src( $image_id, 'single-post-thumbnail' ); 
                $img_srcset = wp_get_attachment_image_srcset( $image_id, 'large' );
                if(!empty($img_srcset)) $img_srcset=' srcset="'.esc_attr( $img_srcset ).'" sizes="(max-width: 100%) 90vw, 850px"';
                //echo "<h2>Set : ".$img_srcset."</h2>";
                $image_alt = get_post_meta( $attachment_id, '_wp_attachment_image_alt', true );
                if(empty($image_alt)) $image_alt=get_the_title($post->ID );
                //if(!empty($img_srcset)) echo "<br>img srcset: successful";
                //else echo "<br>img srcset: unsuccessful (check image size)";
            }
        }

        //if no image found in Media DB, then just display the URL of the first image in content
        $img_start = "<div class=\"post_thumbnail\">";
        $img_end = "</div>";
        if(!$hide_image) {
            if(!isset($image[0])) $image[0]="";//in case it never made it to Media search
            if($image[0]=="") $img_src = $first_img;
            else $img_src = $image[0];
        
            echo $img_start.'<img src="'.$img_src.'" '.$img_srcset.' alt="'.$image_alt.'">'.$img_end;
        }
        else $img_start.'<img src="/wp-content/uploads/no-image-available-gray.jpg" alt="" width:90%;" />'.$img_end;

        //the_post_thumbnail($image_id);
    }
   
    return ""; //$new_img_tag;
}


function woo_post_meta() {
    ?>
    <aside class="post-meta">
        <ul>
            <!--<li class="post-author">
                <?php //echo get_avatar( get_the_author_meta( 'email' ) , '256' ); ?>
                <span><?php //_e( 'by ', 'woothemes' ) . the_author_posts_link(); ?></span>
            </li>
            <li class="post-date">
                <span><?php //echo human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) . __( ' ago', 'woothemes' ); ?></span>
            </li>-->
            <?php 
            $categories = get_the_category();
            $tags = get_the_tags();

             ?>
            <?php if(isset($categories[0])) { ?>
            <li class="post-category">
                <?php
                    if(isset($categories[0]) ) { //& !isset($tags[0])
                        echo '<span>' . __( 'Posted in ', 'woothemes' ) . '</span>';
                    }
                    the_category( ', ');
                ?>
            </li>
            <?php } ?>
            <?php if(isset($hide)) { ?>
            <li class="comments">
                <?php
                    //echo '<span>' . __( 'There are', 'woothemes' ) . '</span>';
                    //comments_popup_link( __( 'No comments', 'woothemes' ), __( '1 comment', 'woothemes' ), __( '% comments', 'woothemes' ) );
                ?>
            </li>
            <?php if(isset($tags[0]))  { ?>
            <li class="tags"><span>
                <?php 
                    if(!isset($categories[0]) && isset($tags[0])) { 
                        echo '<span>' . __( 'Posted in ', 'woothemes' ) . '</span>';
                    }
                    the_tags(); 
                ?>
            </span></li>
            <?php } 
            } ?>
        </ul>
    </aside>

    <?php

    //Shortcodes
    require_once (TEMPLATEPATH . '/functions/shortcodes.php');
}