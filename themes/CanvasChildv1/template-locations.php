<?php
/**
 * @version    1.0.3
-
 * @author     -
 * @copyright  -
 * @license    -
 *
 * Websites: -
 *
 *
 * Template Name: Locations
*/

    get_header();
    global $woo_options;
?>
    <!-- #content Starts -->
    <?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
        <div id="main-sidebar-container">              <?php get_sidebar(); ?>

        </div><!-- /#main-sidebar-container -->         

        <?php get_sidebar( 'alt' ); ?>  

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">                     
<?php
    woo_loop_before();
    
    if (have_posts()) { $count = 0;
        while (have_posts()) { the_post(); $count++; 
        $heading_tag = 'h1';
        if ( is_front_page() ) { $heading_tag = 'h2'; }
        $title_before = '<' . $heading_tag . ' class="title entry-title">';
        $title_after = '</' . $heading_tag . '>';

        $page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );
        woo_post_before();
        ?>
        <div class="locations">
        <article <?php post_class(); ?>>
        <?php
            woo_post_inside_before();
        ?>
        <?php if (has_post_thumbnail( $post->ID ) ){ ?>
        <?php 
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
            $imgback = $image[0];
        }else{
            $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/article-back.jpg";   
        } ?>
        <ul class="breadcrumb">
        <?php bcn_display_list(); ?>
        </ul>
        <header class="article-back">
        
        <div id="map_wrapper">
        <div id="map_canvas" class="mapping"></div>
        </div>
        
        </header>

        <section class="entry">
        <h1><?php the_title(); ?></h1>
        <?php
          the_content( __( 'Continue Reading &rarr;', 'woothemes' ) );
          wp_link_pages( $page_link_args );
        ?>
        



        </section><!-- /.entry -->
        <div class="locationlist">
        <div class="columns large-12">
        <?php $argscustome = array('post_type' => 'location','order'=>'ASC');
              $loopcustome = new WP_Query( $argscustome );    
              while ( $loopcustome->have_posts() ) : $loopcustome->the_post();
              $image_id = get_post_thumbnail_id();
              $image = wp_get_attachment_image_src($image_id, 'large');
              $image_url = $image[0]; //as HREF in your A tag
              $img_srcset = wp_get_attachment_image_srcset( $image_id, 'medium' );
         ?>
         <!--<div class="test"></div>   -->
        <div class="columns large-4">
        <div class="location-content">
        <a href="<?php the_permalink(); ?>"><img src="<?=esc_url( $image_url ); ?>" srcset="<?=esc_attr( $img_srcset ); ?>" sizes="(max-width: 50em) 87vw, 680px" alt="<?=get_post_meta($image_id, '_wp_attachment_image_alt', true); ?>"></a>
        <?php //echo esc_url( get_stylesheet_directory_uri() )."/timthumb.php?w=440&h=281&q=100&src=";?>
        <h3> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php the_excerpt();  ?>
        <div class="maptext">
        <?php 
        if(get_field('location_address')){
        echo $address = get_field('location_address');
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$prepAddr.'&key=AIzaSyBy-5-1oWEz8PfQyQYlGTjogi04gbqqsU8');
        $output= json_decode($geocode);
        $latitude[] = $output->results[0]->geometry->location->lat;
        $longitude[] = $output->results[0]->geometry->location->lng;
        }

        ?>
        </div>
        </div>
        <a class="readmore" href="<?php echo get_permalink();  ?>">Learn More</a>
        </div> 
        <?php endwhile;
        for($i =0;$i< count($latitude);$i++){
            $mark_arr[] = "['',$latitude[$i],$longitude[$i]]"; 
        }
        if(is_array($mark_arr)){
            $mark_str = implode(",",$mark_arr);
        }
          ?>  
        
        </div>
        </div>
        <div class="fix"></div>
        <?php
            woo_post_inside_after();
        ?>
        </article><!-- /.post -->
        </div>
        <?php }  }
        woo_loop_after(); ?>     
        </section><!-- /#main -->
        <?php woo_main_after(); ?>

        </div><!-- /#content -->
        <?php woo_content_after(); ?>
<script> 
jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBy-5-1oWEz8PfQyQYlGTjogi04gbqqsU8&callback=initMap";
    document.body.appendChild(script);
});
// function initialize() {
//     var map;
//     var bounds = new google.maps.LatLngBounds();
//     var mapOptions = {
// 	scrollwheel: false,
//         mapTypeId: google.maps.MapTypeId.ROADMAP
//     };
function initMap() {
        var myLatLng = {lat: 34.111908, lng: -118.622149};
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
	        scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        // Create a map object and specify the DOM element
        // for display.
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          center: myLatLng,
          zoom: 10
        });

                    
    // Display a map on the page
    // map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    // map.setTilt(45);
        
    // Multiple Markers

   
    var markers = [
      <?php echo $mark_str; ?>
    ];
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(10);
        google.maps.event.removeListener(boundsListener);
    });
    
}
</script> 
<?php get_footer(); ?>