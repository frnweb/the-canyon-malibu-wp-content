<?php
    /**
    * Archive Template
    *
    * The archive template is a placeholder for archives that don't have a template file. 
    * Ideally, all archives would be handled by a more appropriate template according to the
    * current page context (for example, `tag.php` for a `post_tag` archive).
    *
    * @package WooFramework
    * @subpackage Template
    */

    global $woo_options;
    get_header();
?>      
<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full">

    <div id="main-sidebar-container">    
        <?php
            if(is_category()){
                $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/blog-back.jpg";
            ?>
            <ul class="breadcrumb">
                <?php bcn_display_list(); ?>
            </ul>
            <header class="article-back" style="background: url('<?php echo $imgback;  ?>') no-repeat center center / cover ">
                <div class="row">
                    <div class="large-12 columns">
                        <h1 class="title entry-title"><?php echo single_cat_title();  ?></h1>
                    </div>
                </div>
            </header>
            <div class="nav-blog-menu">
                <label for="show-menu" class="show-menu">Show Menu</label>
                <input type="checkbox" id="show-menu" role="button">
                <?php wp_nav_menu ('blog-menu'); ?>
            </div>
            <?php
            }
        ?>
        <!-- #main Starts -->
        <?php woo_main_before(); ?>
        <section id="main" class="col-left">

            <?php get_template_part( 'loop', 'archive' ); ?>

        </section><!-- /#main -->
        <?php woo_main_after(); ?>

        <?php get_sidebar(); ?>

    </div><!-- /#main-sidebar-container -->         

    <?php get_sidebar( 'alt' ); ?>       

    </div><!-- /#content -->
	<?php woo_content_after(); ?>
	
<?php get_footer(); ?>