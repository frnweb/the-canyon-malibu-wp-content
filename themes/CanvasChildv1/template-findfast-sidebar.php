<?php
    /**
    * Template Name: Page with Find Fast Sidebar
    *
    * @package WooFramework
    * @subpackage Template
    */
    get_header();
    global $woo_options;

    $heading_tag = 'h1';
    $title_before = '<' . $heading_tag . ' class="title entry-title">';
    $title_after = '</' . $heading_tag . '>'; 
?>
<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full">
    <section id="main">
        <article <?php post_class(); ?>>
            <?php if (has_post_thumbnail( $post->ID ) ){ ?>
                <?php 
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
                    $imgback = $image[0];
                }else{
                    $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/article-back.jpg";   
            } ?>
            <ul class="breadcrumb">
                <?php bcn_display_list(); ?>
            </ul>
            <header class="article-back findfast" style="background: url('<?php echo $imgback; ?>') no-repeat center top">
                <div class="row">
                    <div class="large-12 columns">
                        <?php the_title($title_before, $title_after); ?>
                    </div>
                </div>
            </header>
            <section class="entry columns large-12 small-12">
                <div class="toc">
                    <?php
                        if( have_rows('find_fast_link_with_content') ):
                        $tabInit = 1;
                        while (have_rows('find_fast_link_with_content')) : the_row();
                        $anchorLink = get_sub_field('find_fast_link_title');
                        echo '<a href="#tab_'.$tabInit.'">'.$anchorLink.'</a>';
                        $tabInit++;
                        endwhile;
                        endif;
                    ?>
                </div>
                <?php
                        if( have_rows('find_fast_link_with_content') ):
                        $tabInit = 1;
                        while (have_rows('find_fast_link_with_content')) : the_row();
                        echo '<a name="tab_'.$tabInit.'"></a>';
                        echo $anchorContent = get_sub_field('find_fast_link_content');       
                        $tabInit++;
                        endwhile;
                        endif;
                    ?>
            </section>
        </article>  
    </section>
</div><!-- /#content -->  
<?php woo_content_after(); ?>

<?php get_footer(); ?>