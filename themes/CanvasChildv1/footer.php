		<?php do_action('installation_point'); ?>
<?php
/**
 * Footer Template
 *
 * Here we setup all logic and XHTML that is required for the footer section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options;

 woo_footer_top();
 woo_footer_before();
?>


	<?php //woo_footer_after(); ?>

	</div><!-- /#inner-wrapper -->

</div><!-- /#wrapper -->

<div class="fix"></div><!--/.fix-->
<div class="footer">
<?php
$post_type = get_post_type();
if((!is_single()) && ($post_type != "post")){ 
?>
<div class="socialshare"><div class="col-full">
<div class="columns large-3"><label>Share this page</label></div>
<div class="columns large-9"><?php echo do_shortcode("[wp_social_sharing social_options='linkedin,facebook,twitter' twitter_username='CanyonTreatment' facebook_text='facebook' twitter_text='twitter' linkedin_text='linkedin' icon_order='f,t show_icons='0' before_button_text='' text_position='']");  ?></div>
</div>
</div>
<hr> 
<?php            
    }
?>

<div class="numbersection">
<div class="col-full">
<div class="columns large-6"><label>Private Consultations </label><span><i class="fa fa-phone fa-rotate-45"></i><?php echo do_shortcode('[frn_phone action="Phone Clicks in Header"]'); ?></span></div>
<div class="columns large-6"><a class="canyon-btn" href="<?php  echo get_option('woo_start_admissions_url'); ?>"><?php  echo get_option('woo_start_admissions_text'); ?></a></div>
</div>
</div>
<hr>
<div class="ournetworklogo">
<div class="col-full">
<div class="columns large-12">
<p>Our Network</p>
<ul>
<?php 
        $args = array(
            'post_type' => 'facilities',
            'posts_per_page' => -1,
            'order' => 'ASC',
        );

        $query = new WP_Query($args);

        while($query->have_posts()) :  
        $query->the_post();
?>
     <li class="medium-3 small-6"><a href="<?php the_field('facility_url'); ?>"  rel="nofollow" /><?php the_post_thumbnail('full' ); ?></a></li>
<?php   
                endwhile;
                wp_reset_postdata();
?>
</ul>
</div>
</div>
</div>
<hr>

<div class="address">
<div class="col-full">
<div class="columns large-12">
<div class="columns large-6 small-12 medium-6">
<div class="columns large-12 left-side-col">
<div class="columns large-3 small-5 hide-for-small-only">
<a href="http://thecanyonmalibu.com"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/footeraddress.png" alt="" /></div></a>
<div class="columns large-9 small-12 addressmob"><?php  echo get_option('woo_footer_address'); ?></div></div>
</div>
<div class="columns large-6 small-12 medium-6"> 
<div class="columns large-12 right-side-col medium-12">
<div class="columns large-4 small-6 medium-4 resourse-col">
<div class="first-col">
  <label>Resources</label>
<?php
                $defaults = array(
                'theme_location'  => '',
                'menu'            => 'New Footer 1',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
                );
               wp_nav_menu( $defaults );
?>
</div> 
</div>
<div class="columns large-5 medium-4 secondmobilemenu small-6">
<div class="second-col">
<label>Legal</label>
<?php
                $defaults = array(
                'theme_location'  => '',
                'menu'            => 'New Footer 2',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
                );
               wp_nav_menu( $defaults );
?>
</div>
</div>
<div class="columns large-3 socialiconmob medium-4 small-12">
<a href="<?php  echo get_option('woo_footer_facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="<?php  echo get_option('woo_footer_twitter'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
<a href="https://www.linkedin.com/company/the-canyon/" target="_blank"><i class="fa fa-linkedin"></i></a>
<br>
<a id="bbblink" class="ruhzbas" href="http://www.bbb.org/nashville/business-reviews/drug-abuse-and-addiction-info-and-treatment/foundations-recovery-network-in-brentwood-tn-37038606#bbbseal" title="Foundations Recovery Network, LLC, Drug Abuse & Addiction  Info & Treatment, Brentwood, TN" style="display: block;position: relative;overflow: hidden; width: 100px; height: 45px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="https://seal-nashville.bbb.org/logo/ruhzbas/foundations-recovery-network-37038606.png" width="200" height="45" alt="Foundations Recovery Network, LLC, Drug Abuse & Addiction  Info & Treatment, Brentwood, TN" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2Ffoundations-recovery-network-37038606.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
</div>
</div>
</div>
</div>
<div id="sticky-mobile-strip"></div>
</div>

</div>
<hr>

<div class="col-full footer-accreditations">
    <div class="columns medium-6 small-6">
      <script src="https://static.legitscript.com/seals/2212234.js"></script>
    </div>
    <div class="columns medium-6 small-6">
    <a href="https://www.naatp.org/resources/addiction-industry-directory/19952/the-canyon-at-encino" target="_blank"><img src="https://www.naatp.org//civicrm/file?reset=1&id=4087&eid=201&fcs=1f26e81149c63e3ea4639b16e1ccf9e6a66df075bd944bb7ddb8b7024507c576_1585039261_87600"></a>
    </div>
  </div>

<div id="mobile-strip" class="mobile-strip">
<p>Confidential and Private. We're here to help</p>
<span><i class="fa fa-phone fa-rotate-45"></i><?php echo do_shortcode('[frn_phone action="Phone Clicks in Header"]'); ?></span>
</div>

</div>
<a href="#0" class="cd-top">Top</a>

   <!-- old typekit from ben wright's kit <script src="https://use.typekit.net/ste2ila.js"></script>-->
    <script src="https://use.typekit.net/xuu2vpp.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <script>
        //add close for COVID banner without Foundation support
        var closebtn = document.getElementById('close-btn')
        var banner = document.getElementById('sl_notification-bar');

	      closebtn.onclick = function(event) {
          banner.style.display = "none";
	      }
    </script>

<?php wp_footer(); ?>
<?php woo_foot(); ?>

</body>
</html>