<?php
    /**
    * Header Template
    *
    * Here we setup all logic and XHTML that is required for the header section of all screens.
    *
    * @package WooFramework
    * @subpackage Template
    */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head> 

    <meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
    <title><?php woo_title(); ?></title>
    <?php woo_meta(); ?>
    <?php //echo '   <link rel="pingback" href="'.esc_url( get_bloginfo( 'pingback_url' ) ).'" />'; ?>
        
    <link href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/css/customcp.css?ver=1.2" rel="stylesheet">
    <?php wp_head(); ?>
    <?php woo_head(); ?>

    <style type="text/css">
        .wf-loading {
            font-family: "open-sans";
            visibility: hidden;
        }
        .wf-active {
            visibility: visible;
        }
        #html5-watermark {
            display: none !important;
        }
    </style>
    <script type="text/javascript">
        /*var html5lightbox_options = {
            watermark: "",
            watermarklink: ""
        }; */
    </script>
    <!--[if IE]>
    <style>
    #header .mega-menu-sec ul.mega-menu li.mega-right-menu {   {
    margin-top: -75px !important;
    }
    </style>
    <![endif]-->

    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TMTK4PP');</script>
    <!-- End Google Tag Manager -->
	    
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMTK4PP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php woo_top(); ?>

<div id="wrapper">

<div id="inner-wrapper">

    <?php woo_header_before();
        $settings = woo_get_dynamic_values( array( 'logo' => '' ) );
        $site_title = get_bloginfo( 'name' );
        $site_url = home_url( '/' );
        $site_description = get_bloginfo( 'description' );
    ?>	
    <div id="mobile-header" class="mobile-header col-full" style="display: none;">  
        <div class="mega-menu-sec"><?php echo do_shortcode('[maxmegamenu location=max_mega_menu_1]'); ?></div>
        <div class="mobile-header-icon"><a title="<?php echo esc_attr( $site_title ); ?>" href="<?php echo esc_url( $site_url ); ?>"><img src="/wp-content/uploads/Canyon-Mobile-Header-Icon.png" alt="The Canyon Malibu"></a></div>   
        <div class = "frnphonenumber"><div><h2><?php echo do_shortcode('[frn_phone action="Phone Clicks in Header"]'); ?></h2></div></div> 
        <?php
            if ( ( '' != $settings['logo'] ) ) {
                $logo_url = $settings['logo'];
                if ( is_ssl() ) $logo_url = str_replace( 'http://', 'https://', $logo_url );
            ?>
            <div id="logo">
                <a title="<?php echo esc_attr( $site_title ); ?>" href="<?php echo esc_url( $site_url ); ?>"><img alt="<?php echo esc_attr( $site_title ); ?>" src="<?php echo esc_url( $logo_url ); ?>"></a>
            </div>
            <?php
            }
        ?>
    </div>
    
    <header id="header" class="col-full">
        <div class="columns large-2 logo_sec small-2">
            <?php woo_header_inside(); ?>
        </div>
        <div class="columns large-6 after-header"><?php woo_header_after(); ?></div>
        <div class="columns large-4 contact-info small-8">
            <div class = "frnphonenumber small-6 large-4"><div><h2><i class="fa fa-phone"></i><?php echo do_shortcode('[frn_phone action="Phone Clicks in Header"]'); ?></h2></div> </div>	
            <div class="single-menu small-6 large-6">  <?php
                        $works_link = '';
                        if (!strpos($works_link, 'youtube') > 0) {
                            $works_link = "https://www.youtube.com/watch?v=QiRb78Fgr3k";
                        }
                    ?>
                    <a class="html5lightbox works-link" title="" data-overlayopacity="0.6" href="<?php echo $works_link; ?>">What to expect</a>
            </div>
            <?php /* <div class="mega-menu"><?php //echo do_shortcode('[maxmegamenu location=max_mega_menu_1]'); ?></div>  */ ?>
        </div>
        <div class="mega-menu-sec"><?php wp_nav_menu( array( 'theme_location' => 'max_mega_menu_1' ) ); ?></div>
</header>
<div data-closable class="sl_notification-bar" id="sl_notification-bar" style="background-color:#667f3a;color:#ffffff; padding: 8px 24px;">
	<style type="text/css">
		.sl_notification-bar .sl_inner { width: 100%; max-width: 1080px; margin: 0 auto; font-size: 18px; text-align: center;}
		.sl_notification-bar .sl_button { display: inline-block; padding: 6px 10px; font-size: 15px; border-radius: 3px; color: #fff!important; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2); background-color:#8fb352; letter-spacing: 0; font-weight: 400; margin-left: 8px;}
		.sl_notification-bar .close-button { float:right;display:inline-block; color: #8a8a8a; right: 1rem; top: 0.5rem; font-size: 2em; line-height: 1; cursor: pointer; }
	</style>
        <div id="close-btn" class="close-button" aria-label="Close alert" style="color: #fff;" type="button" data-close>
		    <span aria-hidden="true">&times;</span>
	    </div>
	<div class="sl_inner">
		<span>Committed to Safety: Latest information on COVID-19 Precautions</span><a href="https://www.foundationsrecoverynetwork.com/important-covid-19-update-from-foundations-recovery-network/" class="sl_button">Learn More</a>
	</div>
</div>
<?php

$title = get_the_title();
$iop_kws = array(

	"encino",
	"santa monica",
	"outpatient"

);

$display_banner="y";
foreach($iop_kws as $iop_kw) {
	if(	stripos($title,$iop_kw)!==false ) {
		if($iop_kw=="outpatient" && stripos($title,"inpatient")===false) {
			$display_banner = "n";
		}
		elseif($iop_kw!=="outpatient") $display_banner = "n";
	}
}

if($display_banner=="y") {
	echo '<div class="message-banner mobile-message-banner">
    <p>On November 10, 2018, the Woolsey Fire destroyed The Canyon at Peace Park’s treatment facility. At this time, The Canyon at Peace Park is not accepting patients for any services.
    Click <a href="/message-about-the-canyon-at-peace-park/">here</a> to learn more about our closure or request medical records.</p>
    </div>';
}
?>
</div>
