<?php
    /**
    * Template Name: Home Page
    *
    * @package WooFramework
    * @subpackage Template
    */
    get_header(); 
?>
<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full">

    <div id="main-sidebar-container">              <?php get_sidebar(); ?>

    </div><!-- /#main-sidebar-container -->         

    <?php get_sidebar( 'alt' ); ?>  

    <!-- #main Starts -->
    <?php woo_main_before(); ?>
    <?php
        $hero_image = get_field('hero_image');
        $hero_logo = get_field('hero_image_logo');
        $hero_title = get_field('title_on_hero_image'); 
        $hero_subtitle = get_field('subtitle_on_hero_image'); 
        $tour_link = get_field('link_for_canyon_tour'); 
        $works_link = get_field('link_for_canyon_works'); 
        $treat_link = get_field('link_for_treatment_options');
    ?>
    <section id="main"> 
        <div class="hero" style="background: url(<?php echo $hero_image; ?>) no-repeat fixed right top / cover ;">
            <div class="row VCenter">
                <div class="large-12 columns">
                    <img src="<?php echo $hero_logo; ?>" alt="<?php echo get_bloginfo('name');?>">
                    <hr class="hero_line">
                    <h3 class="hero-title" style="text-align: center; color: #ffffff;"><span style="color: #ffffff;"><?php echo $hero_title; ?></span></h3>
                    <h5 class="hero-subtitle" style="color: #ffffff; text-align: center;"><span style="color: #ffffff;"><?php echo $hero_subtitle; ?></span></h5>   
                </div>
            </div>
            <div class="col-full row VCenter columns hero-link-sec">
                <div class="columns large-4">
                    <a class="tour-link" href="/virtual-tour/" target="_blank">Take a virtual tour</a>
                </div>
                <div class="columns large-4 vedio_box">
                    <?php
                        if (!strpos($works_link, 'youtube') > 0) {
                            $works_link = "https://www.youtube.com/embed/skFrWOJxDW8";
                        }
                    ?>
                    <a class="html5lightbox works-link" title="" data-overlayopacity="0.6" href="<?php echo $works_link; ?>">See why the Canyon works</a>
                    <!--<a class="works-link" href="<?php //echo $works_link; ?>">Watch why the Canyon works</a> -->
                </div>  
                <div class="columns large-4">
                    <a class="treat-link" href="<?php echo $treat_link; ?>">Learn about our Treatment Options</a>

                </div>  
            </div>
        </div>
        <section class="diagnosis-sec">
            <div class="row">
                <h3><?php the_field('diagnosis_title'); ?></h3>
                <p><?php the_field('diagnosis_content'); ?></p>
                <a class="canyon-btn" href="<?php the_field('diagnosis_button_link');  ?>">Learn More</a>
            </div>
        </section> 
        <section class="testi-sec">
            <!--<div class="overlay"></div> -->
            <div class="row"> 
                <?php
                    $args = array(
                    'numberposts' => 1,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'post_type' => 'testimonial' );

                    $recent_testi = wp_get_recent_posts( $args, ARRAY_A );
                    foreach( $recent_testi as $recent ){
                        $testi_title = $recent["post_title"];
                        $testi_content =  $recent["post_content"];
                    }

                ?>
                <div class="large-12 columns testi-wrap">
                    <div class="testi_wrapper">
                        <!--                <div class="testimonial_content">-->
                        <i class="fa fa-quote-left"></i> 
                        <span>
                            <?php echo $testi_content;  ?>
                        </span>
                        <!--                <i class="fa fa-quote-right"></i>-->
                        <!--                </div>-->
                        <div class="testi-auth"><?php echo $testi_title; ?></div>
                    </div>
                </div>
                <a class="canyon-btn" href="<?php the_field('testimonial_page_link'); ?>">More testimonials</a>
            </div>
        </section>      
        <?php
            /*woo_loop_before();

            if (have_posts()) { $count = 0;
            while (have_posts()) { the_post(); $count++;
            woo_get_template_part( 'content', 'page' ); // Get the page content template file, contextually.
            }
            } 

            woo_loop_after();  */
        ?>     
    </section><!-- /#main -->
    <?php woo_main_after(); ?>

    </div><!-- /#content -->
    <?php woo_content_after(); ?>

<?php get_footer(); ?>