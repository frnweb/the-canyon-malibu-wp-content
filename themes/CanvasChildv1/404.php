<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Canvas Child
 * 
 */

 get_header();

//get url
//$wp_query->query_vars['name'];
//remove root install address
//change dashes to spaces
//remove articles and useless words
//that becomes the search


///////
//Prepare the keyword search by pulling words from the URL

//$s = str_replace(get_site_url(),"",$wp_query->query_vars['name']);  //keeping here just in case
$s = str_replace("/","",$_SERVER['REQUEST_URI']);
//$s = trim(preg_replace("/(.*)-html|htm|php|asp|aspx)$/","$1",$s));


	$s = trim(str_replace("-"," ",$s));
	$s = urldecode($s);
	$s = strtolower(preg_replace('/[0-9]+/', '', $s )); //remove all numbers
	$stop_words = array(
		"for",
		"the",
		"and",
		"an",
		"a",
		"is",
		"are",
		"than",
		"that",
		"I",
		"to",
		"on",
		"it",
		"with",
		"can",
		"be",
		"of",
		"get",
		"in",
		"you",
		"from",
		"if",
		"by",
		"so",
		"at",
		"do",
		"&",
		"there",
		"too"
	);
	$i=1;
	foreach($stop_words as $word){
		/*
		//for testing:
		if($i==1) {
			echo $s."<br />";
			echo $word."<br />";
			echo "string: ".strlen($s);
			echo "; word: ".strlen(" ".$word)."<br />";
			echo "position: ".strpos($s,$word." ")."<br />";
			echo "string without word: ".(strlen($s)-strlen(" ".$word))."<br />";
		}
		*/
		$word = trim(strtolower($word));
		$s = str_replace(" ".$word." "," ",$s); ///in the middle
		if(strpos($s,$word." ")===0) $s = str_replace($word." ","",$s); // at the beginning
		if(strpos($s," ".$word)==strlen($s)-strlen(" ".$word)) $s = str_replace(" ".$word,"",$s); // at the end
		$i++;
	}
	




	///////
	//Prepare the list of search results

	//future option: checking if only one page returned, then immediately forwarding the person to that page instead
	//check if relevanssi plugin is activated
	if (function_exists('relevanssi_do_query')) {
		$url_terms_search = new WP_Query();
		$url_terms_search->query_vars['s']				=$s;
		$url_terms_search->query_vars['posts_per_page']	=8;
		$url_terms_search->query_vars['paged']			=0;
		$url_terms_search->query_vars['post_status']	='publish';
		relevanssi_do_query($url_terms_search);
     }
     else {
	    //global $wpdb;
		$url_terms_search = new WP_Query( array( 
			's' => 'treatment', 
			//'page_id' => 26,
			'post_type' => 'any', //array( 'post', 'page' ),
			'posts_per_page' => 8,
			'post_status' => 'publish'
		));
     }

?> 
    
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
	
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    
		
		<?php //print_r($url_terms_search->query);?>
		
		<?php //echo "<h2>Found: ".$url_terms_search->post_count."</h2>"; ?>
            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main" class="col-left">
            <div class="row">
            <div class="columns large-12" style=" padding-top: 3%;">
				<h2><?php _e( 'Whoops!', 'CanvasChildv1' ); ?></h2>
				<br />
				<p><?php _e( 'It looks like something may be wrong with the web address you were using. We are available 24/7 if you have questions about The Canyon, addiction, or what a person can do to overcome addiction. '.do_shortcode('[frn_phone]'), 'CanvasChildv1' ); ?></p>
				<br />
			<?php
                if ( isset($url_terms_search) ) {
                if ( $url_terms_search->have_posts() ) { ?>
				<p><b>Were you looking for one of these?</b></p>
				<ul class="frn_suggestions">
				<?php
				while ( $url_terms_search->have_posts() ) {
					$url_terms_search->the_post();
					?>
					<li>
						<a href="<?php the_permalink();?>"><?php the_title();?></a>
					</li>
					<?php 
				}
				?>
				</ul>
				<br />
				<?php
				} else { ?>
					<p>
						And unfortunately, we can't find any posts that relate to that web address. 
						Take a look at it in the address bar above and see if it looks pretty normal. 
						Make corrections if not and try again.
					</p>
				<?php }
			}
                
            ?>

				<br />
				<p><b><?php _e( '...or maybe try searching:', 'CanvasChildv1' ); ?></b></p>
                <div class="fl" style="margin-bottom:50px;float:none;"><?php get_search_form(); ?></div>
            </div>
            </div>
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>
    
		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>       

    </div><!-- /#content -->
	<?php woo_content_after(); 
	
	/* Restore original Post Data */
	wp_reset_postdata();
	?>
		
<?php get_footer(); ?>