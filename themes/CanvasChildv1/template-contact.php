<?php
    /**
    * Template Name: Contact Us
    *
    * The blog page template displays the "blog-style" template on a sub-page.
    *
    * @package WooFramework
    * @subpackage Template
    */

    get_header();
    global $woo_options;   
?>
       
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">           
		<?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>  

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">                     
<?php
	woo_loop_before();
	
	if (have_posts()) { $count = 0;
		while (have_posts()) { the_post(); $count++;
		$heading_tag = 'h1';
	    if ( is_front_page() ) { $heading_tag = 'h2'; }
		$title_before = '<' . $heading_tag . ' class="title entry-title">';
        $title_after = '</' . $heading_tag . '>';

		$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

		woo_post_before();
?>
	<article <?php post_class(); ?>>
    <?php
        woo_post_inside_before();
    ?>
    <?php if (has_post_thumbnail( $post->ID ) ){ ?>
        <?php 
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
            $imgback = $image[0];
        }else{
            $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/article-back.jpg";   
    } ?>
    <ul class="breadcrumb">
     <?php bcn_display_list(); ?>
    </ul>
    <header class="article-back" style="background: url('<?php echo $imgback; ?>') no-repeat center center / cover">
    <div class="row">
    <div class="large-12 columns">
        <?php the_title( $title_before, $title_after ); ?>
    </div>
    </div>
    </header>

    <section>
    <div class="contactpage"> 
	
	<div class="col-full">
    <div class="columns large-12 headersection">
	
	 <?php
          the_content(); 
        ?>
	</div>	
	</div>
    
	<div class="large-12 middlection">
	<div class="col-full">
	<div class="columns large-12 middlefirst">
	<div class="columns large-7 small-12">
	<?php echo get_field('contact_middle_content'); ?>
	</div>
	<div class="columns large-5 small-12">
	<iframe src="<?php echo get_field('contact_middle_youtube_url'); ?>" width="480" height="272" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</div>
	
	
	</div>
	<div class="columns large-12">
	<?php contact_info(); ?>
	</div>
	</div>
	</div>
	<?php if(get_field('contact_address')){ ?><div class="col-full">
    <div class="columns large-12 headersection">
    <?php echo get_field('contact_address'); ?>
	</div>
	</div><?php } ?>
    <?php if(get_field('contact_map_url')){ ?>
	<div class="large-12 map">
          <iframe src="<?php echo get_field('contact_map_url'); ?>" width="100%" height="250" frameborder="0" ></iframe>
	<!-- <iframe src="<?php echo get_field('contact_map_url'); ?>" width="100%" height="250" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe> -->
            </div>
	<?php } ?>

	</div>
    </section><!-- /.entry -->
    <div class="fix"></div>
    <?php
        woo_post_inside_after();
    ?>
	</article><!-- /.post -->
	<?php
    woo_post_after();
	} }
	woo_loop_after();
?>     
     </section><!-- /#main -->
            <?php woo_main_after(); ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>
<?php get_footer(); ?>