<?php
    /**
    * Template Name: About Page
    *
    * @package WooFramework
    * @subpackage Template
    */
    get_header(); 
?>
<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full"> 
    <?php
        if (has_post_thumbnail($post->ID) ){ 
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
            $imgback = $image[0];
        }else{

            $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/blog-back.jpg";   
        }
    ?>
    <ul class="breadcrumb">
    <?php bcn_display_list(); ?>
    </ul>
    <header class="article-back" style="background: url('<?php echo $imgback;  ?>') no-repeat center center / cover ">
        <div class="row">
            <div class="large-12 columns">
                <h1 class="title entry-title"><?php echo get_the_title($post->ID );  ?></h1>
            </div>
        </div>
    </header>
    <section id="main" class="col-left">
        <div class="row">
            <div id="about-desc" class="desc_section">
                <?php
                    the_content();
                ?>
            </div> 
        </div>
        <section id="about-sub-nav" class="columns large-12 small-12 medium-12">   
            <?php

                // check if the repeater field has rows of data
                if( have_rows('about_sections') ):

                    // loop through the rows of data
                    while (have_rows('about_sections')) : the_row();
                        $image = get_sub_field('section_image');
                        $secTitle =   get_sub_field('section_title'); 
                        $short_content = get_sub_field('section_short_description');
                        $btnLabel = get_sub_field('section_button_label');
                        $btnLink = get_sub_field('link_for_section'); 
                        //                    (str_replace(' ', '-', strtolower($secTitle)));
                        echo '<div class="columns large-6 small-12 sub-nav-sec medium-6">
                        <div class="sec-wrapper" style="background:#667f3a url('.$image.') no-repeat scroll center center / cover;">
                        <h5 class="sec_desc">'.$secTitle.'</h5>
                        <p>'.$short_content.'</p>
                        <a class="canyon-btn" href="'.$btnLink.'">'.$btnLabel.'</a></div>
                        </div>';

                        endwhile;

                    else :

                    // no rows found

                    endif;
            ?>

            <!--            <div class="columns large-6 small-12"></div>
            <div class="columns large-6 small-12"></div>  -->
        </section> 
        <section class="canyon_photos">
            <h2>Photos of the Canyon</h2>
            <?php
                the_field('gallery_section'); 
            ?>
        </section>
        <section class="staff-list">
            <h2>Our Staff</h2>
            <div class="staff-overview row"><?php the_field('staff_overview'); ?></div>
            <div class="staff-members columns large-12 medium-12 small-12">
            <?php
                $query = new WP_Query(array(
                'post_type' => 'staff',
                'posts_per_page' => -1,
                'post_status' => 'publish'
                ));


                while ($query->have_posts()) {
                    $query->the_post();
                    $post_id = get_the_ID();
                    $staff_link = get_the_permalink();
                    $url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id),'medium' );
                    $field_qua = get_field('qualification');  
                    $spost = get_field('title_position');  
                    $sprofile = get_field('linked_in_profile');                 
                    $staffInfo = '<div class="staff-member columns large-3 medium-4 small-12">';
                    if(has_post_thumbnail()){
                        $staffInfo .= '<div class="img-wrap"><a href="'.$staff_link.'" rel="nofollow""><img class="staff-img" src="'.$url[0].'" width="'.$url[1].'" height="'.$url[2].'"></a></div>';
                    }
                    $staffInfo .= '<div class = "staff-info"><span class="sname"><a href="'.$staff_link.'" rel="nofollow"">'.get_the_title().'</a>';

                    if(get_field('qualification')){ 
                        $staffInfo .= ', '.get_field('qualification').'</span>';
                        }
                    else { $staffInfo .='</span>';
                        }

                    $staffInfo .= '<span class="spost">'.$spost.'</span>
                    <!--<a class="sprofile" href="'.$sprofile.'">View my Linked In Profile</a>-->
                    </div>';                   
                    $staffInfo .= '</div>';
                    echo $staffInfo;
                }

                wp_reset_query();
            ?>
            </div>
        </section> 
    </section>
</div><!-- /#content -->  
<?php woo_content_after(); ?>

<?php get_footer(); ?>