<?php 

//Author page for any user in system
//This page can be visited by anyone who knows the link for any user (requires knowing the username).
//Even still, the page is set to "noindex" so search engines will never put the author page in their search results unless "yes" is in the make author public contact field in a user's settings.
//However, no user link will be on any page or post unless a "yes" is placed in the "make author public" contact field for the user.

//Authors ok to make public
	//anna = id=11
	//Siobhan = id=14


get_header(); ?>
	<?php	
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
	
	?>
	<div id="content" class="col-full">
		<div id="author-<?=$curauth->user_login	; ?>">
			<div class="author-content">
				<?php if(function_exists ('userphoto')) userphoto($curauth,'','',array( itemprop=> 'image', width=> '300px')); ?>
				<?php echo $curauth->description; ?>
			</div>
			<div class="contact_info">
				<?php if(trim($curauth->position)!="") : ?><div class="position_title" style="clear:left;" itemprop="jobTitle"><?=$curauth->position; ?></div><?php endif; ?>
				<?php if(trim($curauth->company)!="") : ?><p class="position_company" itemprop="worksFor"><?=$curauth->company; ?></p><?php endif; ?>
				<ul>
					<?php if(trim($curauth->linkedin)!="") : ?><li class="facebook" itemprop="url"><a href="<?=$curauth->linkedin; ?>"><!--Connect on Linked In--></a></li><?php endif; ?>
					<?php if(trim($curauth->googleplus)!="") : ?><li class="google" itemprop="url"><a href="<?=$curauth->googleplus; ?>?rel=author"><!--Follow on Google+--></a></li><?php endif; ?>
					<?php if(trim($curauth->slideshare)!="") : ?><li class="slideshare" itemprop="url"><a href="<?=$curauth->slideshare; ?>"><!--Follow on SlideShare--></a></li><?php endif; ?>
				</ul>
			</div>
			<br />
		</div>
		<div style="clear:both;"></div>
		 <?php wp_reset_postdata(); ?>
	
		<?php 
		$author_id=$curauth->ID;
		// the query
		$args = array(
			'post_type' => 'any',
			'author' => $author_id,
			'nopaging' => true 
		);
		$the_query = new WP_Query( $args ); 
		if($the_query) { ?>
		<div id="frn_related_posts" style="margin:20px 0;">
		<h2>Posts by <?=$curauth->first_name; ?>:</h2>

		<ul>
		<?php if ( $the_query->have_posts() ) : ?>

		  <!-- the loop -->
		  <?php 
		  $count=1;
		  $show_posts=8;
		  while ( $the_query->have_posts() ) : $the_query->the_post(); 
		  //if( $count<>9) {
				$colors = array("d9d9d9", "bababa", "a2a2a2");
				$color = array_rand($colors, 1);
				$color_hover_styles = array("color_hvr_grn", "color_hvr_bl", "color_hvr_org", "color_hvr_rd", "color_hvr_ppl");  //grays: array("d9d9d9", "bababa", "a2a2a2");
				$color_hover_style = array_rand($color_hover_styles, 1);
				//echo $color;
				//echo $count;
		?>
			<a href="<?=get_permalink(); ?>"><li class="<?=$color_hover_styles[$color_hover_style]; ?>" style="background-color:#<?=$colors[$color]; ?>;"><div><?php echo trim(substr(get_the_title(), 0, 52)); if(strlen(get_the_title())>52) echo "…"; ?></div></li></a>
			<?php
			
			if($count==$show_posts) { ?>
				<div id="related_posts_seelink" style="margin-left:5px;"><a href="javascript:related_show_more()">See All</a></div>
				<span id="related_posts_more" style="display:none;">
			<?php }
			
			if($count==$the_query->post_count) echo "</span>";
			
			$count++;
		  //}
		  ?>
		  
		  <?php endwhile; 
		  
			//echo $the_query->post_count . "; " . $count;
		  
		  if($count>$show_posts) {
		  ?>
			<script type="text/javascript">
				function related_show_more() {
					document.getElementById('related_posts_more').style.display='block';
					document.getElementById('related_posts_seelink').style.display='none';
				}
			</script>
		  <?php } ?>
		  <div style="clear:both;"></div>
		  <!-- end of the loop -->

		  <?php wp_reset_postdata(); ?>

		<?php else:  ?>
		  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; 
		
		} //ends check if author has any posts?>

		</ul>
		</div>
	</div>

<?php get_footer(); ?>