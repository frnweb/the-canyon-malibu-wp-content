<?php
/**
 * Post Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */

$settings = array(
				'thumb_w' => 9999,
				'thumb_h' => 300,
				'thumb_align' => 'aligncenter',
				'post_content' => 'excerpt',
				'comments' => 'both'
				);

$settings = woo_get_dynamic_values( $settings );

$title_before = '<h1 class="title entry-title">';
$title_after = '</h1>';

if ( ! is_single() ) {
	//$title_before = '<h3 class="title entry-title">';
	$title_before .= '<a href="' . esc_url( get_permalink( get_the_ID() ) ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';
	$title_after = '</a>';
	//$title_after .= '</h3>';
}

$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

woo_post_before();
?>
<article <?php post_class(); ?>>
    <h2 class="post-title">
    <?php the_title($title_before,$title_after); ?>
    </h2>
<?php
$post_num = get_post_meta( $post->ID, 'incr_number', true );
//woo_post_inside_before();
woo_post_meta();

/*
////Prepare Key Image
$post_type = get_post_type();
if((is_single()) && ($post_type == "post")){
if (has_post_thumbnail( $post->ID ) ): ?>
  <?php 
  	$image_id = get_post_thumbnail_id( $post->ID );
  	$image = wp_get_attachment_image_src( $image_id, 'single-post-thumbnail' ); 
  	$img_srcset = wp_get_attachment_image_srcset( $image_id, 'medium' );
  ?>
  
  <img src="<?=$image[0]; ?>" srcset="<?=esc_attr( $img_srcset ); ?>" sizes="(max-width: 100%) 90vw, 850px" alt="<?php get_the_title($post->ID );?>" width="100%">
<?php endif; 

}
else  { //if (('content' != $settings['post_content'] && ! is_singular()) && (($post_num-2)%3 == 0) && (!is_archive() || is_archive()) )
*/
	
	
	if (has_post_thumbnail() ): 

		echo "
		<div class=\"post_thumbnail\">
		";

	  	$image_id = get_post_thumbnail_id( get_the_id() );
	  	//echo "featured image present (".$image_id.")";
	  	$image = wp_get_attachment_image_src( $image_id, 'single-post-thumbnail' ); 
	  	$img_srcset = wp_get_attachment_image_srcset( $image_id, 'large' );
	  	if(!empty($img_srcset)) $img_srcset='srcset="'.esc_attr( $img_srcset ).'" sizes="(max-width: 100%) 50vw, 650px"';
	  	$image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
        if(empty($image_alt)) $image_alt=get_the_title(get_the_id());
	  ?>
	  		<img src="<?=$image[0]; ?>" <?=$img_srcset;?> alt="<?=esc_attr($image_alt);?>">
	  	<?php 
	  	echo "</div>";
	  /*
	  	$image_id = get_post_thumbnail_id();
	  	$image = wp_get_attachment_image_src( $image_id, 'single-post-thumbnail' ); 
	  	$img_srcset = wp_get_attachment_image_srcset( $image_id, 'medium' );
	  ?>
	  
	  <img src="<?=$image[0]; ?>" srcset="<?=esc_attr( $img_srcset ); ?>" sizes="(max-width: 100%) 90vw, 850px" alt="<?php get_the_title($post->ID );?>" width="100%">
	
	  <?php 
	  */

	elseif(! is_singular()):
		//echo "image in content (no featured image)";
		//woo_image( 'width=' . esc_attr( $settings['thumb_w'] ) . '&height=' . esc_attr( $settings['thumb_h'] ));
		catch_that_image(); //our custom function
	endif; 

	
	
//}
//else{
//    echo "<div class='blankspace'></div>";
//}

/////End preparing Key Image



?>

<section class="entry">
<?php
if ( 'content' == $settings['post_content'] || is_single() ) { the_content( __( 'Continue Reading &rarr;', 'woothemes' ) ); } else { the_excerpt(); }
if ( 'content' == $settings['post_content'] || is_singular() ) wp_link_pages( $page_link_args );
?>
	</section><!-- /.entry -->
	<div class="fix"></div>
<?php
woo_post_inside_after();
?>
</article><!-- /.post -->

<?php
woo_post_after();
$comm = $settings['comments'];
if ( ( 'post' == $comm || 'both' == $comm ) && is_single() ) { comments_template(); }
?>
