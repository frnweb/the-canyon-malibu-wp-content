<article class="post">
	<h3 class="notfound">Unfortunately, we were unable to find what you were looking for.</h3>
<p class="notfound">You can try searching again or view our latest posts below.</p>
<div class="searchnotfound"><?php get_search_form(); ?></div>
<?php 
			
			/////
			// RELATED POSTS, Option 2: all pages in current post's category
			/////

				
					//The following are the default variables for get_posts in case we want to change them
					$args = array(
					'category'         => '',
					'posts_per_page'   => 11,
					'offset'		   => 1,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'include'          => '',
					'exclude'          => '',
					'meta_key'         => '',
					'meta_value'       => '',
					'post_type'        => array( 'post', 'page' ),
					'post_mime_type'   => '',
					'post_parent'      => '',
					'post_status'      => 'publish',
					'suppress_filters' => true ); 
				 $posts_array = get_posts( $args ); 
				//posts per page limits how many are returned at bottom of page
				//offset says that if there is only one in the list, then don't return anything anyway
				//echo count($posts_array);  //removed since post_array is empty if only one is returned
				//print_r ($posts_array);

				if( $posts_array ) : ?>
					<div id="frn_related_posts">

						<div class="rp_list">
						<ul>

						<?php 
						$count=1;
						foreach ( $posts_array as $post_new ) : setup_postdata( $post_new );
							//skips current post in list
							if( ($post_new->ID == get_option('page_on_front'))) $count=$count-1;
							if( ($post_new->ID != get_option('page_on_front')) && $count<10) {
								//$colors = array("8fb352", "8fb352", "8fb352");
								//$color = array_rand($colors, 1);
								//$color_hover_styles = array("color_hvr_grn", "color_hvr_bl", "color_hvr_org", "color_hvr_rd", "color_hvr_ppl");  //grays: array("C3DBB4", "D3E2B6", "A8DBA8");
								//$color_hover_style = array_rand($color_hover_styles, 1);
								//echo $color;
								//echo get_option('page_on_front');

						?>
							<a href="<?php echo get_permalink($post_new->ID); ?>">
								<li class="color_hvr_grn" style="background-color:#8fb352;"><?//=$color_hover_styles[$color_hover_style]; ?>
									<div class="positioning"><div class="overflow_limit"><?php 
										echo trim(substr($post_new->post_title, 0, 52)); if(strlen($post_new->post_title)>52) echo "…"; 
										?></div></div>
								</li>
							</a>
							<?php
							$count++;
							}
							else $count=$count-1;
						endforeach;
						?>
						</ul>
						</div>
					</div>
					<?php
				endif;
				wp_reset_postdata();

				//print_r($posts_array);
				$post=null;

			?> 
			
		
		
<!-- EndRelatedPosts -->
</article>