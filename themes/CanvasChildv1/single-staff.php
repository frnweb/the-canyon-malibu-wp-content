<?php
    /**
    *
    * The blog page template displays the "blog-style" template on a sub-page.
    *
    * @package WooFramework
    * @subpackage Template
    */

    get_header();
    global $woo_options;   
?>
      
    <!-- #content Starts -->
    <?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
        <div id="main-sidebar-container">              <?php get_sidebar(); ?>

        </div><!-- /#main-sidebar-container -->         

        <?php get_sidebar( 'alt' ); ?>  

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">                     
<?php
    woo_loop_before();
    
    if (have_posts()) { $count = 0;
        while (have_posts()) { the_post(); $count++; 
        $heading_tag = 'h1';
        if ( is_front_page() ) { $heading_tag = 'h2'; }
        $title_before = '<' . $heading_tag . ' class="title entry-title">';
        $title_after = '</' . $heading_tag . '>';
        $urlstaff = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID) );
		$field_qua = get_field('qualification');  
        $spost = get_field('title_position');  
        $page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );
        //woo_post_before();
        ?>
        <div class="locations">
        <article <?php post_class(); ?>>
        <?php
            //woo_post_inside_before();
        ?>
        <?php if (has_post_thumbnail( $post->ID ) ){ ?>
        <?php 
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
            $imgback = $image[0];
        }else{
            $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/article-back.jpg";   
        } ?>
        <ul class="breadcrumb">
        <?php bcn_display_list(); ?>
        </ul>
 
        <div class="staffdetail">
        
        <div class="col-full">
        <div class="columns large-12 staffsection">
        <div class="staffheadersection">
	
        <div class ="staff-info">
		<div class="columns large-9 small-12">
		<?php if(has_post_thumbnail()){ ?>
        <img class="authoreimg" src="<?php echo $urlstaff[0]; ?>" width="<?php echo $urlstaff['1']; ?>" height="<?php echo $urlstaff['2']; ?>" />
        <?php } ?>
		<h1><?php echo get_the_title();?><span class="qualification"><?php if(get_field('qualification')){ echo ', '; echo get_field('qualification'); }else{ echo ""; } ?></span> </h1>
        <span class="spost"><?php echo $spost; ?></span>
		</div>
		
		<div class="columns large-3 small-6">
		<?php if(get_field('linked_in_profile')){ ?>
		<a target="_blank" href="<?php echo get_field('linked_in_profile'); ?>">
		<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/linkedin.png"  alt=""/>
		</a>
		<?php } ?>
		<?php if(get_field('google_plus_profile')){ ?>
		<a target="_blank" href="<?php echo get_field('google_plus_profile'); ?>">
		<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/google-plus.png"  alt=""/>
		</a>
		<?php } ?>
		<?php if(get_field('twitter_profile')){ ?>
		<a target="_blank" href="<?php echo get_field('twitter_profile'); ?>">
		<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/twitter.png"  alt=""/>
		</a>
		<?php } ?>
		<?php if(get_field('web_profile')){ ?>
		<a target="_blank" href="<?php echo get_field('web_profile'); ?>">
		<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/web.png"  alt=""/>
		</a>
		<?php } ?>
		</div>
		</div>
	    </div>
		<div class="fix"></div>

        <div class="staffmiddlesection">
		<hr>
		<?php the_content(); ?>
		</div>
		
        </div>
        </div>

        <div class="fix"></div>
        <?php
            woo_post_inside_after();
        ?>
        </article><!-- /.post -->
        </div>
        <?php }  }
        woo_loop_after(); ?>     
        </section><!-- /#main -->
        <?php woo_main_after(); ?>

        </div><!-- /#content -->
        <?php woo_content_after(); ?>

<?php get_footer(); ?>