<?php
    /**
    *
    * The blog page template displays the "blog-style" template on a sub-page.
    *
    * @package WooFramework
    * @subpackage Template
    */

    get_header();
    global $woo_options;   
?>

<!-- #content Starts -->
<?php woo_content_before(); ?>
<div id="content" class="col-full">

    <div id="main-sidebar-container">              <?php get_sidebar(); ?>

    </div><!-- /#main-sidebar-container -->         

    <?php get_sidebar( 'alt' ); ?>  

    <!-- #main Starts -->
    <?php woo_main_before(); ?>
    <section id="main">                     
        <?php
            woo_loop_before();

            if (have_posts()) { $count = 0;
                while (have_posts()) { the_post(); $count++; 
                    $heading_tag = 'h1';
                    if ( is_front_page() ) { $heading_tag = 'h2'; }
                    $title_before = '<' . $heading_tag . ' class="title entry-title">';
                    $title_after = '</' . $heading_tag . '>';

                    $page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );
                    //woo_post_before();
                ?>
                <div class="locations">
                    <article <?php post_class(); ?>>
                        <?php
                            //woo_post_inside_before();
                        ?>
                        <?php if (has_post_thumbnail( $post->ID ) ){ ?>
                            <?php 
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
                                $imgback = $image[0];
                            }else{
                                $imgback = trailingslashit( get_stylesheet_directory_uri() ) ."assets/images/article-back.jpg";   
                        } ?>
                        <ul class="breadcrumb">
                            <?php bcn_display_list(); ?>
                        </ul>
                        <header class="article-back" style="background: url('<?php echo $imgback; ?>') no-repeat center center / cover">
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php the_title( $title_before, $title_after ); ?>
                                </div>
                            </div>
                        </header>
                        <div class="locationdetail">

                            <?php if( have_rows('facility_photos') ):  ?>         
                                <div class="columns large-12 facilityphotos">

                                    <h2>Facility Photos</h2>
                                    <?php 
                                        while ( have_rows('facility_photos') ) : the_row(); 
                                            $image = get_sub_field('facility_photo');
                                            //echo "<h2>".$image['sizes'][ 'medium' ]."</h2>";
                                            $img_srcset = wp_get_attachment_image_srcset( $image['id'], 'medium' );
                                        ?>
                                        <div class="columns large-3 small-6"><img src="<?=$image['sizes'][ 'medium' ];?>" srcset="<?=esc_attr( $img_srcset ); ?>" sizes="(max-width: 50em) 87vw, 680px" alt="<?=$image['alt'];?>" /></div>
                                        <?php //echo esc_url( get_stylesheet_directory_uri() ); /timthumb.php?w=320&h=180&q=100&src=<?=$image['url']; ?>
                                        <?php endwhile; ?>
                                  <div class="columns large-12"><?php if(get_field('view_gallery_url')){ ?><a class="canyon-btn" href="<?php the_field('view_gallery_url');  ?>">view gallery</a><?php } ?></div>

                                </div>
                                <?php endif; ?> 

                            <div class="fix"></div>

                            <!--        <div class="col-full">-->
                            <section class="entry columns large-12 aboutsection">
                                <div class="content_sec">
                                <h2>About <?php the_title(); ?></h2>
                                <?php the_content(); ?>
                                </div>
                            </section>
                            <!--        </div>-->

                            <?php if(get_field('brochure_title')){ ?>
                                <section class="diagnosis-sec">
                                <div class="row">
                                    <h3><?php echo get_field('brochure_title'); ?></h3>
                                    <p><?php echo get_field('brochure_description'); ?></p>
                                    <?php if(get_field('brochure_file')){ ?><a class="canyon-btn" href="<?php echo get_field('brochure_file'); ?>" target="_blank">Download Brochure</a><?php } ?>
                                </div> 
                                </section><?php } ?>
                            <div class="fix"></div>
                            <div class="col-full">
                                <div class="columns large-12 videosection">
                                    <h2><?php echo get_field('video_title'); ?></h2>
                                    <?php if(get_field('video_url')){ ?><iframe src="<?php echo get_field('video_url'); ?>" width="100%" height="420" frameborder="0"></iframe><?php } ?>

                                    <?php if( have_rows('treatment_programs_section') ):
                                            while ( have_rows('treatment_programs_section') ) : the_row(); ?>
                                            <h3><?php the_sub_field('location_detail_footer_section_title'); ?></h3>
                                            <?php the_sub_field('location_detail_footer_section_description'); ?>
                                            <?php endwhile; endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="fix"></div>
                        <?php
                            woo_post_inside_after();
                        ?>
                    </article><!-- /.post -->
                </div>
                <?php }  }
            woo_loop_after(); ?>     
    </section><!-- /#main -->
    <?php woo_main_after(); ?>

        </div><!-- /#content -->
        <?php woo_content_after(); ?>

<?php get_footer(); ?>