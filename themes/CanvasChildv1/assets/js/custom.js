function mobile_header(){
    //    header_height = (jQuery("#header-container").height()) - 4;
    //    header_height = (jQuery("#mobile-header").height());
    //    alert(header_height);
    //    jQuery("#content").css({'margin-top':header_height});         
    if(jQuery("#wpadminbar").length > 0){
        wp_ht = jQuery("#wpadminbar").height();
        jQuery("#header-container").css({"top":wp_ht});  
    }else{
        jQuery("#header-container").css({"top":"0"});    
    }
    windowWidth = jQuery(window).width(); 
    if(windowWidth <= 767){
        jQuery("#header").hide();
        jQuery("#mobile-header").show();
        jQuery(".mega-mobilemenu ul").hide();
//        jQuery("#mobile-strip").show(); 
        jQuery(".photospace_res .thumbs_wrap a.thumb").click(function(){
            jQuery('html, body').animate({
                scrollTop: jQuery('.gal_content').offset().top - 300
            }, 500);
        });
        header_height = (jQuery("#mobile-header").height());
        jQuery("#content").css({'margin-top':header_height});
    }
    else{
        jQuery("#mobile-header").hide();
        jQuery(".mega-mobilemenu").removeClass("active");
        jQuery("#header").show();
        jQuery(".mega-mobilemenu ul").show();  
//        jQuery("#mobile-strip").hide(); 
        jQuery("#content").css({'margin-top':"56px"});     
    }

}
function mobile_strip(){
    // var targetOffset = jQuery("#sticky-mobile-strip").offset().top;
    /*var $w = jQuery(window).scroll(function(){
        var targetOffset = jQuery("#sticky-mobile-strip").offset().top;
        if ( $w.scrollTop() > targetOffset ) { 
//            jQuery("#mobile-strip").css({"position":"relative"},{"bottom":"62px"}); 
        } else {
//            jQuery("#mobile-strip").css({"position":"fixed"}); 
        }
    });*/
}
function setNavigation(){
    //    var slide_ht = (jQuery(".photospace_res .gal_content").height()) / 2;  
    var slide_ht = (jQuery(".photospace_res .slideshow img").height()) / 2;  
    //    alert(jQuery(".photospace_res .slideshow img").height());
    var winwidth = jQuery(document).width();
    //    alert(slide_ht);
    jQuery(".photospace_res .controls").css({'top':slide_ht});  
}
function equalHt(){
    jQuery('.middlection').each(function(){  
        var highestBox = 0;
        jQuery('.large-4', this).each(function(){
            if(jQuery(this).height() > highestBox) {
                highestBox = jQuery(this).height(); 
            }

        });
        jQuery('.large-4',this).height(highestBox);    

    });
    /*jQuery('.locationlist').each(function(){
        var highestBox = 0;    
        jQuery('.maptext', this).each(function(){
            if(jQuery(this).height() > highestBox) {
                highestBox = jQuery(this).height(); 
            }
        });  
        jQuery('.maptext',this).height(highestBox);       
    });*/ 
    /*jQuery('.locationlist').each(function(){
        var highestBox = 0;    
        jQuery('.locationlist .location-content > p', this).each(function(){
            if(jQuery(this).height() > highestBox) {
                highestBox = jQuery(this).height(); 
            }
        });  
        jQuery('.locationlist .location-content > p',this).height(highestBox);       
    });*/ 
    jQuery('.staff-members').each(function(){
        var highestBox = 0;    
        jQuery('.staff-member', this).each(function(){
            if(jQuery(this).height() > highestBox) {
                highestBox = jQuery(this).height(); 
            }
        });  
        jQuery('.staff-member',this).height(highestBox);       
    });
    jQuery('#about-sub-nav').each(function(){
        var highestBox = 0;    
        jQuery('.sub-nav-sec .sec-wrapper', this).each(function(){
            if(jQuery(this).height() > highestBox) {
                highestBox = jQuery(this).height(); 
            }
        });  
        jQuery('.sub-nav-sec .sec-wrapper',this).height(highestBox);       
    }); 
}
function setPadding(){
    windowWidth = jQuery(window).width();

    if((jQuery('section.entry').length > 0) && (jQuery("section.entry .toc").length == 0)){
        if(windowWidth >= 767){ 
            jQuery('section.entry').css({'padding':'5% 12%'}); 
        }else{
            jQuery('section.entry').css({'padding':'2% 25px'}); 
        } 
    }  

}
function stickToc(){
    windowWidth = jQuery(window).width(); 
    windowHeihgt =  jQuery(window).height();    
    windowHeihgt = windowHeihgt - 60; 
    if((jQuery("#tocsidebar").length > 0) && (windowWidth > 1024)){ 
//    alert("big"); 
        jQuery("#tocsidebar").css({'height':windowHeihgt});  
        jQuery(".scroll-pane").css({'height':windowHeihgt});      
        jQuery("#tocsidebar").sticky({
            topSpacing: 58,
            zIndex:2,
            stopper: ".footer"
        });
        //    if(sidebarHeight > windowHeihgt){
        jQuery('.scroll-pane').jScrollPane();
        //    }
        jQuery('.scroll-pane').css("width", ""); 
//        jQuery('#tocsidebar').css("position", "fixed"); 
//        jQuery('#tocsidebar').css("top", "58px"); 
    }
    else{  
        sidebarHeight = jQuery("#tocsidebar").height(); 
        jQuery('#tocsidebar').css("position", "static");  
        jQuery("#tocsidebar").css({'height':sidebarHeight}); 
//        jQuery("#tocsidebar").removeAttr('style');
    }
}
function resizeToc(){    
    windowWidth = jQuery(window).width();
//    var offset = jQuery("#tocsidebar").offset().top;     
    jQuery(window).scroll(function(){        
        if((windowWidth < 1024)){
            sidebarHeight = jQuery("#tocsidebar").height();  
            jQuery('#tocsidebar').css("position", "static"); 
            jQuery('#tocsidebar').css("top", ""); 
//            jQuery("#tocsidebar").removeAttr('style');  
//            jQuery("#tocsidebar").css({'height':sidebarHeight});   
        }else{
//           jQuery('#tocsidebar').css("position", "fixed"); 
//           jQuery('#tocsidebar').css("top", "58px");  
        }
        
    }); 
}
function scrollSide(){
    windowWidth = jQuery(window).width(); 
    windowHeihgt =  jQuery(window).height(); 
    //    var contnt_ht =  (jQuery(".content-wrap")[0].scrollHeight) - 985; 
    if((jQuery("#tocsidebar").length > 0) && (windowWidth > 1024)){
        jQuery("#tocsidebar").css({'height':windowHeihgt}); 
        var offset = jQuery("#tocsidebar").offset(); 
        var topPadding = 60;
        var targetOffset = jQuery(".footer").offset().top; 
        jQuery(window).scroll(function() {
            if(jQuery(window).scrollTop() > targetOffset){
                jQuery("#tocsidebar").stop().animate({
                    marginTop: 0
                }); 
            }
            else if (jQuery(window).scrollTop() > offset.top) {
                jQuery("#tocsidebar").stop().animate({
                    marginTop: jQuery(window).scrollTop() - offset.top + topPadding
                });
            } else {
                jQuery("#tocsidebar").stop().animate({
                    marginTop: 0
                });
            };
        });          
    }
}
function sizeofContainer(){
    windowWidth = jQuery(window).width();
    if((jQuery("section.entry .toc").length == 0) && (jQuery('.page section.entry').length > 0) && (windowWidth > 1200) && (jQuery('.page section.entry .content_sec').length == 0) && (jQuery('section.entry.aboutauthore_welcome').length ==0)){
        jQuery( ".page section.entry" ).wrapInner( "<div class='content_sec'></div>");
    }   
}
function onScroll(event){
    var scrollPos = jQuery(document).scrollTop();
    jQuery('.tocsidebar .toc a').each(function () {
        var currLink = jQuery(this);
        var refElement = jQuery(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            jQuery('.tocsidebar .toc a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}
jQuery(document).ready( function() {
    jQuery('.vedio_box a').click(function(){
        jQuery(".html5lightbox").html5lightbox(); 
    });   
    var bcrumb = obj_name.templateNm; 
    jQuery('.breadcrumb li span[property="name"]').each(function(){
        if((jQuery(this).html().indexOf(bcrumb) > -1) && (bcrumb != "")){
            jQuery(this).html(bcrumb);
        }
    });
    mobile_header();
    setPadding();
//    mobile_strip();
    equalHt();
    if(jQuery("section.entry .toc").length > 0){
        jQuery("section.entry").addClass("columns large-12 small-12");
        if(jQuery(".toc h2").first().length > 0){
            jQuery(".toc h2").first().hide();
        }
        jQuery(".toc").wrap('<div id="tocsidebar" class="columns large-3 small-12 tocsidebar"></div>');
        jQuery("#tocsidebar").wrapInner("<div class='scroll-pane'></div>");
        if(jQuery('section.entry').children().first().hasClass('tocsidebar')==false){
            jQuery('section.entry').children().first().nextUntil('.tocsidebar').andSelf().wrapAll('<div class="outerwrapper row" />');
        }
        jQuery("section.entry > div.tocsidebar ~ *").wrapAll("<div class='content-wrap columns large-9 small-12'></div>");
        //        var contnt_ht =  (jQuery(".content-wrap")[0].scrollHeight) - 985;
        //        alert(contnt_ht);
        //        jQuery("div.tocsidebar").css({"height":contnt_ht}); 
        jQuery(".entry .tocsidebar .toc a:first-child").addClass("active");
        //        var obj = jQuery(".tocsidebar .toc a");
        //        var arr = jQuery.makeArray( obj );
        /* var findFastlinkarr = [];  
        jQuery(".tocsidebar .toc a").each(function(){
        var linkhref = jQuery(this).attr("href");
        findFastlinkarr.push(linkhref);
        }); */

        //        alert(findFastlinkarr);  
        /*var p = jQuery('a[name=detox]').offset().top - 150;
        jQuery(window).scroll(function(){ 
        if(jQuery(window).scrollTop() > p){
        //               jQuery("a[href=detox]").addClass("active");
        jQuery("a[href$=detox]").css({'color':"blue"});   
        }else{
        jQuery("a[href$=detox]").css({'color':"red"}); 
        }
        });*/
        jQuery(".tocsidebar .toc a").click(function(){
            jQuery('html, body').animate({
                scrollTop: jQuery('[name="' + jQuery.attr(this, 'href').substr(1) + '"]').offset().top - 100
            }, 500);            
            if(jQuery(".tocsidebar .toc a").hasClass("active")){
                jQuery(".tocsidebar .toc a").removeClass("active");  
            }
            jQuery(this).addClass("active"); 

            return false;
        });


    }
    jQuery('a.readmore').each(function(){
        jQuery(this).addClass('canyon-btn'); 
    });
    sizeofContainer();

    /*if((jQuery('section.entry').length > 0) && (jQuery("section.entry .toc").length == 0)){
    jQuery('section.entry').css({'padding':'5% 12%'}); 
    }*/

    jQuery(".accordion").accordion({ 
        header: "h3",          
        autoheight: false,
        active: false,
        alwaysOpen: false,
        fillspace: false,
        collapsible: true,
        heightStyle: "content",
        icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" }
    });
    jQuery(".box.green.half-left").addClass("columns large-6 small-12");
    jQuery(".box.half-right.green").addClass("columns large-6 small-12");

    var divs = jQuery("#mega-menu-wrap-max_mega_menu_1 #mega-menu-max_mega_menu_1 > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item.mega-resources-megamenu li");
    for(var i = 0; i < divs.length; i+=3) {
        divs.slice(i, i+3).wrapAll('<ul class="inner-resource columns large-2"></ul>');
    }
    jQuery(".box.half-right.green").addClass("columns large-6 small-12");
    jQuery( "<i class='fa fa-plus'></i>" ).insertAfter( ".mega-mobilemenu > a" );
    jQuery(".mega-mobilemenu i").click(function(){
        jQuery(".mega-mobilemenu ul").not(jQuery(this).next()).slideUp('fast',function(){
            jQuery(this).prev().removeClass("fa-minus");  
            jQuery(this).prev().addClass("fa-plus");  
        });
        jQuery(this).next().slideToggle('fast',function(){
            if (jQuery(this).is(":visible")){
                jQuery(this).prev().addClass("fa-minus"); 
                jQuery(this).prev().removeClass("fa-plus");
            }else{    
                jQuery(this).prev().removeClass("fa-minus");  
                jQuery(this).prev().addClass("fa-plus"); 
            }
        }); 
    });
    jQuery("#navigation .nav-search a.search-contents").click(function(){
        if(jQuery("#header #navigation ul.nav-search ul").css('display') == 'none'){
            jQuery("#header #navigation ul.nav-search ul").css({"display":"block"});   
        }else{
            jQuery("#header #navigation ul.nav-search ul").css({"display":"none"});    
        }

    });
    jQuery(document).mouseup(function (e)
    {
        var container = jQuery("#header #navigation ul.nav-search ul");

        if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
            container.css({"display":"none"});
        }
    });
    if(jQuery(".one-third").length > 0){
        jQuery(".one-third").addClass("columns large-4 small-12");
    }
    if(jQuery(".two-third").length > 0){
        jQuery(".two-third").addClass("columns large-8 small-12");
    }
    if(jQuery(".one-half").length > 0){
        jQuery(".one-half").addClass("columns large-6 small-12");
    }
    if((jQuery(".author p").length > 0) ){
        jQuery(".author p").find('br').remove();
        if(jQuery(".author p strong").length > 0){
            jQuery(".author p strong").parent().addClass("text");
            var a = jQuery('.author .text').first().contents().eq(0).text();
            var $tmp = jQuery('.author .text').first().contents().eq(0),dia = document.createTextNode('Authored By:');

            $tmp.length > 0 ? $tmp.replaceWith(dia) : jQuery(dia).insertBefore(".author .text");
            jQuery(".author .text strong").before("<br />");
            jQuery('.author a[rel~="external"]').parent().addClass("author-social");
            jQuery('.author a[rel~="external"]').each(function(){
                var socila_nm = jQuery(this).text();
                jQuery(this).text("");
                socila_nm = socila_nm.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, ' ').toLowerCase();
                jQuery(this).addClass(socila_nm);
            });
        }

    }
    jQuery("div.box:contains('Citations')").addClass("citations");
    jQuery("div.box:contains('Related Topics:')").addClass("related-topics");  
    jQuery(".box.citations a[name^='_ftn']").each(function(){
        var ctnum = jQuery(this).text().replace(/[\[\]']+/g,'');
        jQuery(this).text(ctnum);
    });
    var offset = 300,
    offset_opacity = 1200,
    scroll_top_duration = 700,
    $back_to_top = jQuery('.cd-top');
    jQuery(window).scroll(function(){
        ( jQuery(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( jQuery(this).scrollTop() > offset_opacity ) { 
            $back_to_top.addClass('cd-fade-out');
    }});
    $back_to_top.on('click', function(event){
        event.preventDefault();
        jQuery('body,html').animate({
            scrollTop: 0 ,
        }, scroll_top_duration
        );
    });
    //    scrollSide();
    var addrlht = (jQuery(".footer .address").height()) + (jQuery(".footer .copyright").height());
    jQuery('body .box.gray p.sub-text').prev('p').css({'margin':'0'}); 
    stickToc(); 
    windowWidth = jQuery(window).width(); 
    if(windowWidth < 1024){ resizeToc();  
    }
    jQuery('a[name^="_ftn"]').click(function(){
    jQuery('html, body').animate({
        scrollTop: jQuery('[name="' + jQuery.attr(this, 'href').substr(1) + '"]').offset().top - 80
    }, 500);
    return false;
});
});
jQuery(window).resize(function() {
    mobile_header(); 
    setNavigation(); 
//    mobile_strip();
    setPadding();  
    equalHt();
    //    scrollSide();
    sizeofContainer();
    stickToc();
    windowWidth = jQuery(window).width(); 
    if(windowWidth < 1024){    
        sidebarHeight = jQuery("#tocsidebar .toc").height();         
        jQuery('#tocsidebar').css("position", "static");  
        jQuery("#tocsidebar").css({'height':sidebarHeight});
        jQuery(".scroll-pane").css({'height':sidebarHeight}); 
        jQuery('#tocsidebar').css("top", ""); 
        resizeToc();    
    }
    
/*windowWidth = jQuery(window).width();   
if(windowWidth > 1024){
windowHeihgt =  jQuery(window).height(); 
jQuery("#tocsidebar").css({'height':windowHeihgt});
jQuery('.scroll-pane').jScrollPane();   
}*/
   
//    resizeToc(); 
    //    videoHeight();

});
jQuery(window).load(function(){ 

    jQuery(".photospace_res .nav-controls .next > span").text("");
    jQuery(".photospace_res .nav-controls .prev > span").text("");
    /*if(jQuery(".content-wrap").length > 0){
    var contnt_ht =  (jQuery(".content-wrap")[0].scrollHeight);
    jQuery("div.tocsidebar").css({"height":contnt_ht}); 
    }  */
    setNavigation();
     
});

jQuery(window).load(function(){        
    jQuery('.canyonmessage').show();
    jQuery('body.home').addClass('message-open');
  }); 

 jQuery(".canyonmessage__inner__button, .canyonmessage__inner__button--alt").click(function(){
    jQuery('body.home').removeClass('message-open');
    jQuery('.canyonmessage').hide();
  });