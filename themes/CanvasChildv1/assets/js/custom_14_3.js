function mobile_header(){
    header_height = jQuery("#header-container").height();
    jQuery("#content").css({'margin-top':header_height});
    if(jQuery("#wpadminbar").length > 0){
        wp_ht = jQuery("#wpadminbar").height();
        jQuery("#header-container").css({"top":wp_ht});  
    }else{
        jQuery("#header-container").css({"top":"0"});    
    }
    windowWidth = jQuery(window).width(); 
    if(windowWidth <= 767){
        jQuery("#header").hide();
        jQuery("#mobile-header").show();

        jQuery(".mega-mobilemenu ul").hide();
       
    }
    else{
        jQuery("#mobile-header").hide();
        jQuery(".mega-mobilemenu").removeClass("active");
        jQuery("#header").show();
        jQuery(".mega-mobilemenu ul").show();

    }
}
jQuery(document).ready( function() {
    mobile_header();
    if(jQuery("section.entry .toc").length > 0){
        jQuery("section.entry").addClass("columns large-12 small-12");
        if(jQuery(".toc h2").first().length > 0){
            jQuery(".toc h2").first().hide();
        }
        jQuery(".toc").wrap('<div class="columns large-3 small-12 tocsidebar"></div>');
        jQuery("section.entry > div.tocsidebar ~ *").wrapAll("<div class='content-wrap columns large-9 small-12'></div>");
        var contnt_ht1 =  (jQuery(".content-wrap")[0].scrollHeight);
        var contnt_ht = jQuery("section.entry").outerHeight( true );
        jQuery("div.tocsidebar").css({"height":contnt_ht});
        jQuery(".tocsidebar .toc a").click(function(){
            /*var toclink = jQuery(this).attr('href');
            if (toclink.indexOf("/") >= 0){
                var toclink = toclink.split("/").pop(); 
            } */
            jQuery('html, body').animate({
                scrollTop: jQuery('[name="' + jQuery.attr(this, 'href').substr(1) + '"]').offset().top - 100
            }, 500);
            /*jQuery('html, body').animate({
            scrollTop: jQuery(toclink).offset().top
            }, 500); */
            return false;
        }); 
        var contnt_ht =  (jQuery(".content-wrap")[0].scrollHeight) - 985;
        //        alert(contnt_ht);
        jQuery("div.tocsidebar").css({"height":contnt_ht});
    }
    if((jQuery('section.entry').length > 0) && (jQuery("section.entry .toc").length == 0)){
        jQuery('section.entry').css({'padding':'5% 12%'}); 
    }
    jQuery(".accordion").accordion({ 
        header: "h3",          
        autoheight: false,
        active: false,
        alwaysOpen: false,
        fillspace: false,
        collapsible: true,
        heightStyle: "content",
        icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" }
    });
    jQuery(".box.green.half-left").addClass("columns large-6 small-12");
    jQuery(".box.half-right.green").addClass("columns large-6 small-12");
    var divs = jQuery("#mega-menu-wrap-max_mega_menu_1 #mega-menu-max_mega_menu_1 > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item.mega-resources-megamenu li");
    for(var i = 0; i < divs.length; i+=3) {
        divs.slice(i, i+3).wrapAll('<ul class="inner-resource columns large-2"></ul>');
    }
    jQuery(".box.half-right.green").addClass("columns large-6 small-12");
    var divs = jQuery("#mega-menu-wrap-max_mega_menu_1 #mega-menu-max_mega_menu_1 > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item.mega-resources-megamenu li");
    for(var i = 0; i < divs.length; i+=3) {
        divs.slice(i, i+3).wrapAll('<ul class="inner-resource columns large-2"></ul>');
    }
    jQuery(".box.half-right.green").addClass("columns large-6 small-12");
    jQuery( "<i class='fa fa-plus'></i>" ).insertAfter( ".mega-mobilemenu > a" );
    jQuery(".mega-mobilemenu i").click(function(){
       
        jQuery(".mega-mobilemenu ul").not(jQuery(this).next()).slideUp();
       // jQuery(".mega-mobilemenu i").not(jQuery(this).next()).slideUp().removeClass('fa-plus-circle').addClass('fa-minus-circle');

        //jQuery(this).find('.mega-mobilemenu i').toggleClass('fa-plus-circle fa-minus-circle');
        jQuery(this).next().slideToggle();   
        
    });
});
jQuery(window).resize(function() {
    mobile_header();  
});